#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="/style.css"/>
#+title: Presentation - Sprites
#+author: Fortias Matheo

** Fonctionnalités désirées
Dans ce mini-projet, j'ai voulu créé une animation 2D en utilisant les sprites. Les sprites sont des images qui contiennent plusieurs images.
On peut les utiliser pour créer des animations en affichant une image à la fois. Cela permet de créer des animations plus fluides et plus rapides.
J'ai également voulu créé un paysage avec différents plans en mouvement.

** Fonctionnement du programme

Avant la boucle du programme, j'ai charger les différents plans du paysage et les sprites. J'ai également initialisé les variables nécessaires pour les déplacements des plans
et des sprites (source, destination, etat, ...). Il est important de remarquer ici que j'ai utilisé deux images par plans pour permettre un parallaxe fluide et sans trous.

Dans ma boucle du programme, j'ai donc gérer le defilement, à différentes vitesses, des différents plans, en prenant soin de les remettre à leur position initiale (qui correspond à
0 - largeur_de_la_fenetre) lorsqu'ils sortent de l'écran (que ce soit pour l'originale ou pour la copie).

#+BEGIN_SRC cpp
        /* Defilement du premier fond et de son bis */
        if (destination1.x - speed1 > 0 - window_dimensions.w)
            destination1.x -= speed1;
        else
            destination1.x = window_dimensions.w - speed1 -1;

        if (destination1_bis.x - speed1 > 0 - window_dimensions.w)
            destination1_bis.x -= speed1;
        else
            destination1_bis.x = window_dimensions.w - speed1 -1;

        /* Defilement du deuxieme fond et de son bis */
        if (destination2.x - speed2 > 0 - window_dimensions.w)
            destination2.x -= speed2;
        else
            destination2.x = window_dimensions.w - speed2 - 1;

        if (destination2_bis.x - speed2 > 0 - window_dimensions.w)
            destination2_bis.x -= speed2;
        else
            destination2_bis.x = window_dimensions.w - speed2 - 1;

        /* Defilement du troisieme fond et de son bis */
        if (destination3.x - speed3 > 0 - window_dimensions.w)
            destination3.x -= speed3;
        else
            destination3.x = window_dimensions.w - speed3 - 1;
        
        if (destination3_bis.x - speed3 > 0 - window_dimensions.w)
            destination3_bis.x -= speed3;
        else
            destination3_bis.x = window_dimensions.w - speed3 - 1;
#+END_SRC

J'ai ici voulu utiliser simplement les deux premieres images de la deuxieme ligne.

Image de l'aventurier :

[[aventurier.png]]

Pour ce qui est des sprites, il a fallu alterner entre deux images adjacentes afin de donner l'illusion de course du personnage.
Le personnage ne se deplace pas dans la fenetre, c'est le mouvement vers l'arriere du background qui donne l'impression
que le personnage avance.

#+BEGIN_SRC cpp
        /* Mouvement de l'aventurier */
        state_av.x += offset_x;                     // On passe à la vignette suivante dans l'image
        state_av.x %= (source_av.w - 7*offset_x);   // La vignette qui suit celle de fin de ligne est celle de début de ligne
#+END_SRC

Ici, offset correspond à la largeur ou la hauteur d'une vignette de l'image de l'aventurier. On peut donc voir que l'on passe à la vignette suivante dans l'image,
et que l'on revient à la première vignette sans parcourir les 7 dernieres.


Commande pour convertir en HTML avec le css:
#+BEGIN_SRC sh
pandoc sprites_matheo.org -o sprites_matheo.html -s
#+END_SRC