#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

/* VARIABLES GLOBALES */
SDL_Window   * fenetre   = NULL; // fenetre utilisee par le programme
int            largeur    = 870;  // largeur de la fenetre
int            hauteur   = 480;  // hauteur de la fenetre
SDL_Renderer   *rendu = NULL;   // moteur de rendu SDL
SDL_Rect fenetre_dimensions = {0};

/* FONCTIONS */
void dessine(SDL_Renderer *rendu, int largeur, int hauteur){
    SDL_Rect carre;
    SDL_Rect carre2;
    SDL_Rect carre3;

    SDL_Delay(100);
    SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255);
    int portion_x = largeur/5;
    int portion_y = hauteur/3;
    carre.x = 2*portion_x;
    carre.y = 2*portion_y;
    carre.w = carre.h = largeur/50;
    SDL_RenderFillRect(rendu, &carre);
    SDL_RenderPresent(rendu);
    SDL_Delay(100);


    carre2.x = 2*portion_x + 0.5*portion_x;
    carre2.y = 2*portion_y;
    carre2.w = carre2.h = largeur/50;
    SDL_RenderFillRect(rendu, &carre2);
    SDL_RenderPresent(rendu);
    SDL_Delay(100);

    carre3.x = 3*portion_x;
    carre3.y = 2*portion_y;
    carre3.w = carre3.h = largeur/50;
    SDL_RenderFillRect(rendu, &carre3);
    SDL_RenderPresent(rendu);
    SDL_Delay(100);
}

int main(int argc, char **argv){

    (void) argc;
    (void) argv;
    int tourne = 1;
    SDL_Event evenement;

    if (SDL_Init(SDL_INIT_VIDEO) < 0){
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    SDL_Window   * fenetre;
    fenetre = SDL_CreateWindow("Fenêtre Principale", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                largeur, hauteur, 
                SDL_WINDOW_RESIZABLE); 
        
    if (fenetre == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    rendu = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED );
    if (rendu == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    while(tourne){
        while(SDL_PollEvent(&evenement)){
            switch(evenement.type){
                case SDL_WINDOWEVENT:
                    switch (evenement.window.event){
                        case SDL_WINDOWEVENT_CLOSE:
                            printf("Appui sur la croix\n");
                            tourne = 0;
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            largeur = evenement.window.data1;
                            hauteur = evenement.window.data2;
                            break;
                    }
                    break;
                case SDL_QUIT:
                    printf("On quitte\n");
                    tourne = 0;
                    break;
                default:
                    break;
            }
            SDL_GetWindowSize(fenetre, &fenetre_dimensions.w, &fenetre_dimensions.h);
            printf("%d %d\n",largeur, hauteur);
            SDL_SetRenderDrawColor(rendu, 0, 0, 0, 255);
            SDL_RenderClear(rendu);
            dessine(rendu, fenetre_dimensions.w, fenetre_dimensions.h);
        }
        SDL_Delay(1);
    }

    SDL_DestroyRenderer(rendu);
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
    return 0;
}