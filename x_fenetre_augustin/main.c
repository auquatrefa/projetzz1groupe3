#include <SDL2/SDL.h>
#include <stdio.h>

/* VARIABLES GLOBALES */
SDL_Window   * fenetre   = NULL; // fenetre utilisee par le programme
int            largeur    = 870;  // largeur de la fenetre
int            hauteur   = 480;  // hauteur de la fenetre
SDL_Renderer   *rendu = NULL;   // moteur de rendu SDL
SDL_Rect ecran;
//SDL_DisplayMode ecran2;
SDL_Window *fenetre1;
SDL_Window *fenetre2;
SDL_Window *fenetre3;

/* FONCTIONS */
void fenetreMultiClonage(int x, int y){
    int largeur_sous_fenetres = x/2;
    int hauteur_sous_fenetres = y/2;
    fenetre1 = SDL_CreateWindow("Sous-fenêtre 1", 0, 0, 
                largeur_sous_fenetres, hauteur_sous_fenetres, 
                SDL_WINDOW_RESIZABLE); 
    fenetre2 = SDL_CreateWindow("Sous-fenêtre 2", x, 0, 
                largeur_sous_fenetres, hauteur_sous_fenetres, 
                SDL_WINDOW_RESIZABLE); 
    fenetre3 = SDL_CreateWindow("Sous-fenêtre 3", 0, y, 
                largeur_sous_fenetres, hauteur_sous_fenetres, 
                SDL_WINDOW_RESIZABLE);     
    if (fenetre1 == 0 || fenetre2 == 0 || fenetre3 == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }
}

int main(int argc, char ** argv){

    (void) argc;
    (void) argv;
    SDL_Event evenement;
    int tourne = 1;
    int separable = 1;

    if (SDL_Init(SDL_INIT_VIDEO) < 0){
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    SDL_Window   * fenetre;
    fenetre = SDL_CreateWindow("Fenêtre Principale", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                largeur, hauteur, 
                SDL_WINDOW_RESIZABLE); 
        
    if (fenetre == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    rendu = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED );
    if (rendu == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    SDL_GetDisplayUsableBounds(0, &ecran);
    //SDL_GetCurrentDisplayMode(0, &ecran2);

    while(tourne){
		while(SDL_PollEvent(&evenement)){
			switch(evenement.type){
				case SDL_WINDOWEVENT:
    				switch (evenement.window.event)  {
						case SDL_WINDOWEVENT_CLOSE:  
    						printf("appui sur la croix\n");
    						break;
    					case SDL_WINDOWEVENT_SIZE_CHANGED:
    						largeur = evenement.window.data1;
    						hauteur = evenement.window.data2;
    						printf("Nouvelle taille : %d %d\n", largeur, hauteur);
                            break;
    					default:
                            SDL_RenderClear(rendu);
                            SDL_SetRenderDrawColor(rendu, 156, 36, 127, 255);
                            SDL_RenderDrawPoint(rendu,largeur/2,hauteur/2);
                            SDL_RenderPresent(rendu);
    				}   
    				break;
				case SDL_MOUSEBUTTONDOWN:
                    puts("Appui souris");
                    if(separable){
                        separable = 0;
                        int haut, gauche, bas, droit;
                        SDL_GetWindowBordersSize(fenetre, &haut, &gauche, &bas, &droit);
                        int x = ecran.w - (2*gauche + 2*droit);
                        int y = ecran.h - (2*haut + 2*bas);
                        fenetreMultiClonage(x,y);
                        SDL_SetWindowSize(fenetre, x/2, y/2);
                        SDL_SetWindowPosition(fenetre, x, y); 
                    }
                    break;
                case SDL_KEYDOWN:
                    switch (evenement.key.keysym.sym){
                        case SDLK_SPACE:
                            puts("Appui barre d'espace");
                            separable = 1;
                            if(fenetre1 && fenetre2 && fenetre3){
                                SDL_DestroyWindow(fenetre1);
                                SDL_DestroyWindow(fenetre2);
                                SDL_DestroyWindow(fenetre3);
                                SDL_SetWindowSize(fenetre, largeur, hauteur);
                                SDL_SetWindowPosition(fenetre, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
                            }
                            break;
                        
                        default:
                            puts("Appui sur n'importe quelle autre touche");
                            SDL_RenderClear(rendu);
                            SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255);
                            SDL_RenderDrawPoint(rendu,largeur/2,hauteur/2);
                            SDL_RenderPresent(rendu);
                            break;
                    }
                    break;
				case SDL_QUIT : 
    				printf("On quitte\n");  
    				tourne = 0;
			}
		}
		SDL_Delay(1);
	}


    SDL_DestroyRenderer(rendu);
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
    return 0;
}