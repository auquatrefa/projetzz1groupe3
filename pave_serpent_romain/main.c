#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    SDL_Window *window = NULL;     // initialisation fenetre
    SDL_Renderer *renderer = NULL; // initialisation renderer

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n",
                SDL_GetError()); // l'initialisation de la SDL a échoué
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    window = SDL_CreateWindow("Pavé de serpents", (mode.w - 800) / 2, (mode.h - 600) / 2, 800, 600,
                              SDL_WINDOW_RESIZABLE);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",
                SDL_GetError()); // la création de la fenêtre a échoué
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (renderer == NULL)
    {
        SDL_Log("Error : SDL renderer creation - %s\n",
                SDL_GetError()); // la création du renderer a échoué
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_bool
        program_on = SDL_TRUE,   // programme doit continuer
        event_utile = SDL_FALSE; // evenement en cours
    SDL_Event event;

    SDL_Rect rectangle;
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    rectangle.w = 10;
    rectangle.h = 10;
    rectangle.x = 395;
    rectangle.y = 295;

    int r = 255, g = 0;

    while (program_on)
    {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, r, g, 0, 255);
        SDL_RenderFillRect(renderer, &rectangle);
        SDL_RenderPresent(renderer);
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        { // si on a un evenement dans la file
            switch (event.type)
            {
            case SDL_QUIT:
                program_on = SDL_FALSE;
                event_utile = SDL_TRUE;
                break;
            case SDL_MOUSEWHEEL:
                if ((event.wheel.y > 0) && (g < 255))
                {
                    g += 15;
                    r -= 15;
                }
                else if (event.wheel.y < 0 && (r < 255))
                {
                    r += 15;
                    g -= 15;
                }
                event_utile = SDL_TRUE;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_RIGHT:
                    if (rectangle.x < 800 - rectangle.w)
                    {
                        rectangle.x += 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_LEFT:
                    if (rectangle.x > 0)
                    {
                        rectangle.x -= 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_UP:
                    if (rectangle.y > 0)
                    {
                        rectangle.y -= 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_DOWN:
                    if (rectangle.y < 600 - rectangle.h)
                    {
                        rectangle.y += 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_z:
                    if (rectangle.y > 0)
                    {
                        rectangle.y -= 5;
                        rectangle.h += 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_s:
                    if (rectangle.y < 600 - rectangle.h)
                    {
                        rectangle.h += 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_q:
                    if (rectangle.x > 0)
                    {
                        rectangle.x -= 5;
                        rectangle.w += 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_d:
                    if (rectangle.x < 800 - rectangle.w)
                    {
                        rectangle.w += 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_i:
                    if (rectangle.h > 10)
                    {
                        rectangle.h -= 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_k:
                    if (rectangle.h > 10)
                    {
                        rectangle.y += 5;
                        rectangle.h -= 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_j:
                    if (rectangle.w > 10)
                    {
                        rectangle.w -= 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_l:
                    if (rectangle.w > 10)
                    {
                        rectangle.x += 5;
                        rectangle.w -= 5;
                    }
                    event_utile = SDL_TRUE;
                    break;
                }
            }
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}