#ifndef sprites_h
#define sprites_h

#include <SDL2/SDL.h>

typedef struct donnees_fenetre_s
{
    SDL_Window *window;
    SDL_Renderer *renderer;
    int width;
    int height;
} donnees_f_t;


void afficheFenetre();

#endif