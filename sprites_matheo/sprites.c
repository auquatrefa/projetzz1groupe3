#include <stdio.h>
#include <stdlib.h>

#include "sprites.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/*****************************************************************************************/
/* Fonction end_sdl                                                                      */
/*                                                                                       */
/* Cette fonction permet de quitter la SDL en cas d'erreur                               */
/*                                                                                       */
/* Paramètres :                                                                          */
/*      - char ok : 0 si tout s'est bien passé, 1 sinon                                  */
/*      - char const *msg : message à afficher en cas d'erreur                           */
/*      - SDL_Window *window : fenêtre à fermer                                          */
/*      - SDL_Renderer *renderer : renderer à fermer                                     */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void end_sdl(char ok,            // fin normale : ok = 0 ; anormale ok = 1
             char const *msg,    // message à afficher
             SDL_Window *window, // fenêtre à fermer
             SDL_Renderer *renderer)
{ // renderer à fermer
    char msg_formated[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formated, msg, 250);
        l = strlen(msg_formated);
        strcpy(msg_formated + l, " : %s\n");

        SDL_Log(msg_formated, SDL_GetError());
    }

    if (renderer != NULL)
    {                                  // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(renderer); // Attention : on suppose que les NULL sont maintenus !!
        renderer = NULL;
    }
    if (window != NULL)
    {                              // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(window); // Attention : on suppose que les NULL sont maintenus !!
        window = NULL;
    }

    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/*****************************************************************************************/
/* Fonction load_texture_from_image                                                      */ 
/*                                                                                       */
/* Cette fonction permet de charger une texture à partir d'une image                     */
/*                                                                                       */
/* Paramètres :                                                                          */
/*      - char *file_image_name : nom du fichier image à charger                         */
/*      - SDL_Window *window : fenêtre où afficher l'image                               */
/*      - SDL_Renderer *renderer : renderer où afficher l'image                          */
/*                                                                                       */
/* Retour : SDL_Texture *                                                                */
/*                                                                                       */
/*****************************************************************************************/
SDL_Texture *load_texture_from_image(char *file_image_name, SDL_Window *window, SDL_Renderer *renderer)
{
    SDL_Surface *my_image = NULL;   // Variable de passage
    SDL_Texture *my_texture = NULL; // La texture

    my_image = IMG_Load(file_image_name); // Chargement de l'image dans la surface
                                          // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL,
                                          // uniquement possible si l'image est au format bmp */
    if (my_image == NULL)
        end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire
    if (my_texture == NULL)
        end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void defilement(SDL_Window *window, SDL_Renderer *renderer)
{
    /* Création des textures */
    SDL_Texture *premier_fond = load_texture_from_image("premier_fond.png", window, renderer);
    SDL_Texture *premier_fond_bis = load_texture_from_image("premier_fond.png", window, renderer);
    SDL_Texture *deuxieme_fond = load_texture_from_image("deuxieme_fond.png", window, renderer);
    SDL_Texture *deuxieme_fond_bis = load_texture_from_image("deuxieme_fond.png", window, renderer);
    SDL_Texture *troisieme_fond = load_texture_from_image("troisieme_fond.png", window, renderer);
    SDL_Texture *troisieme_fond_bis = load_texture_from_image("troisieme_fond.png", window, renderer);
    SDL_Texture *aventurier = load_texture_from_image("aventurier.png", window, renderer);

    /* Creation des rectangles de destination et source pour le premier fond */
    SDL_Rect
        source1 = {0},            // Rectangle définissant la zone totale du fond 1
        source2 = {0},            // Rectangle définissant la zone totale du fond 2
        source3 = {0},            // Rectangle définissant la zone totale du fond 3
        source_av = {0},          // Rectangle définissant la zone totale de l'aventurier
        destination1 = {0},       // Rectangle définissant où la zone_source1 doit être déposée dans le renderer
        destination1_bis,         // Rectangle définissant où la zone_source1_bis doit être déposée dans le renderer
        destination2 = {0},       // Rectangle définissant où la zone_source2 doit être déposée dans le renderer
        destination2_bis,         // Rectangle définissant où la zone_source2_bis doit être déposée dans le renderer
        destination3 = {0},       // Rectangle définissant où la zone_source3 doit être déposée dans le renderer
        destination3_bis,         // Rectangle définissant où la zone_source3_bis doit être déposée dans le renderer
        destination_av = {0},     // Rectangle définissant où la zone_source de l'aventurier doit être déposée dans le renderer
        state = {0},              // Rectangle de la vignette en cours pour le fond (il n'yen a qu'une)
        state_av = {0},           // Rectangle de la vignette en cours pour l'aventurier
        window_dimensions = {0};  // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur;

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h); // Récupération des dimensions de la fenêtre

    SDL_QueryTexture(premier_fond, NULL, NULL, &source1.w, &source1.h); // Récupération des dimensions du premier fond
    SDL_QueryTexture(deuxieme_fond, NULL, NULL, &source2.w, &source2.h); // Récupération des dimensions du deuxieme fond
    SDL_QueryTexture(troisieme_fond, NULL, NULL, &source3.w, &source3.h); // Récupération des dimensions du troisieme fond
    SDL_QueryTexture(aventurier, NULL, NULL, &source_av.w, &source_av.h);// Récupération des dimensions de l'aventurier

    /* Gestion de l'etat (le même pour les différents fonds) */
    state.x = 0; // en debut de ligne
    state.y = 0; // en debut de colonne
    state.w = window_dimensions.w;
    state.h = window_dimensions.h;

    /* Gestion de la destination pour le fond 1 */
    destination1.w = window_dimensions.w;
    destination1.h = window_dimensions.h;    
    destination1_bis.w = window_dimensions.w;      
    destination1_bis.h = window_dimensions.h;
    destination1.x = 0;
    destination1.y = 0;
    destination1_bis.x = window_dimensions.w;
    destination1_bis.y = 0;     

    /* Gestion de la destination pour le fond 2 */
    destination2.w = window_dimensions.w;   
    destination2.h = window_dimensions.h;   
    destination2_bis.w = window_dimensions.w;    
    destination2_bis.h = window_dimensions.h; 
    destination2.x = 0;
    destination2.y = 0;
    destination2_bis.x = window_dimensions.w;
    destination2_bis.y = 0;     

    /* Gestion de la destination pour le fond 3 */
    destination3.w = window_dimensions.w;      
    destination3.h = window_dimensions.h;     
    destination3_bis.w = window_dimensions.w;     
    destination3_bis.h = window_dimensions.h;     
    destination3.x = 0;
    destination3.y = 0;
    destination3_bis.x = window_dimensions.w;
    destination3_bis.y = 0;

    /* Gestion taille des frames de l'aventurier */
    int nb_images = 9;                      // Il y a 9 vignettes dans la ligne de l'image qui nous intéresse
    int offset_x = source_av.w / nb_images, // La largeur d'une vignette de l'image, marche car la planche est bien réglée
         offset_y = source_av.h / 3;        // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
    state_av.x = 0;                           // La première vignette est en début de ligne
    state_av.y = offset_y;                    // On s'intéresse à la 4ème ligne, le bonhomme qui court
    state_av.w = offset_x;                    // Largeur de la vignette
    state_av.h = offset_y;                    // Hauteur de la vignette

    /* Gestion vitesse de defilement et zoom */
    int speed1 = 17; // vitesse du premier fond et de l'aventurier
    int speed2 = 6;  // vitesse du deuxieme fond
    int speed3 = 1;  // vitesse du troisieme fond
    float zoom = 1.6; // zoom de l'aventurier

    /* Gestion de la destination de l'aventurier */
    destination_av.w = offset_x * zoom;       // Largeur du sprite à l'écran
    destination_av.h = offset_y * zoom;       // Hauteur du sprite à l'écran
    destination_av.y = 0.785*(window_dimensions.h - destination_av.h); // La course se fait au niveau du sol 
    destination_av.x = 0.5*(window_dimensions.w - destination_av.w); // La course se fait en milieu d'écran (en vertical)

    /* Ecoute et gestion des évènements */
    SDL_bool
        program_on = SDL_TRUE,
        event_utile = SDL_FALSE;
    SDL_Event event;

    while (program_on)
    {
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT: // on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE;
                event_utile = SDL_TRUE;
                break;

            default: // Les évènements qu'on n'a pas envisagé
                break;
            }
        }

        /* Defilement du premier fond et de son bis */
        if (destination1.x - speed1 > 0 - window_dimensions.w)
            destination1.x -= speed1;
        else
            destination1.x = window_dimensions.w - speed1 -1;

        if (destination1_bis.x - speed1 > 0 - window_dimensions.w)
            destination1_bis.x -= speed1;
        else
            destination1_bis.x = window_dimensions.w - speed1 -1;

        /* Defilement du deuxieme fond et de son bis */
        if (destination2.x - speed2 > 0 - window_dimensions.w)
            destination2.x -= speed2;
        else
            destination2.x = window_dimensions.w - speed2 - 1;

        if (destination2_bis.x - speed2 > 0 - window_dimensions.w)
            destination2_bis.x -= speed2;
        else
            destination2_bis.x = window_dimensions.w - speed2 - 1;

        /* Defilement du troisieme fond et de son bis */
        if (destination3.x - speed3 > 0 - window_dimensions.w)
            destination3.x -= speed3;
        else
            destination3.x = window_dimensions.w - speed3 - 1;
        
        if (destination3_bis.x - speed3 > 0 - window_dimensions.w)
            destination3_bis.x -= speed3;
        else
            destination3_bis.x = window_dimensions.w - speed3 - 1;


        /* Mouvement de l'aventurier */
        state_av.x += offset_x;                     // On passe à la vignette suivante dans l'image
        state_av.x %= (source_av.w - 7*offset_x);   // La vignette qui suit celle de fin de ligne est celle de début de ligne


        SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle

        /* On met les différents fonds et l'aventurier dans le renderer */
        SDL_RenderCopy(renderer, troisieme_fond, &state, &destination3);
        SDL_RenderCopy(renderer, troisieme_fond_bis, &state, &destination3_bis); 
        SDL_RenderCopy(renderer, deuxieme_fond, &state, &destination2); 
        SDL_RenderCopy(renderer, deuxieme_fond_bis, &state, &destination2_bis); 
        SDL_RenderCopy(renderer, premier_fond, &state, &destination1); 
        SDL_RenderCopy(renderer, premier_fond_bis, &state, &destination1_bis); 
        SDL_RenderCopy(renderer, aventurier, &state_av, &destination_av); 

        /* On affiche le rendu */
        SDL_RenderPresent(renderer); 

        SDL_Delay(100); // Petite pause

        SDL_RenderClear(renderer); // Effacer la fenêtre
    }

    /* On libere les différentes textures */
    SDL_DestroyTexture(premier_fond);
    SDL_DestroyTexture(premier_fond_bis);
    SDL_DestroyTexture(deuxieme_fond);
    SDL_DestroyTexture(deuxieme_fond_bis);
    SDL_DestroyTexture(troisieme_fond);
    SDL_DestroyTexture(troisieme_fond_bis);
    SDL_DestroyTexture(aventurier);
}

/*********************************************************************************/
/* Fonction afficheFenetre                                                       */
/*                                                                               */
/* Fonction permettant d'afficher une fenêtre                                    */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void afficheFenetre()
{
    donnees_f_t ma_fenetre;
    SDL_DisplayMode mode;

    /* Création de la SDL */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT", ma_fenetre.window, ma_fenetre.renderer);

    SDL_GetCurrentDisplayMode(0, &mode); // données de l'ecran

    /* Création de la fenêtre */
    ma_fenetre.window = SDL_CreateWindow("Premier dessin", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1025, 1025, SDL_WINDOW_OPENGL);
    if (ma_fenetre.window == NULL)
        end_sdl(0, "ERROR WINDOW CREATION", ma_fenetre.window, ma_fenetre.renderer);

    /* Création du renderer */
    ma_fenetre.renderer = SDL_CreateRenderer(ma_fenetre.window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (ma_fenetre.renderer == NULL)
        end_sdl(0, "ERROR RENDERER CREATION", ma_fenetre.window, ma_fenetre.renderer);

    defilement(ma_fenetre.window, ma_fenetre.renderer);

    SDL_DestroyRenderer(ma_fenetre.renderer);
    SDL_DestroyWindow(ma_fenetre.window);
    IMG_Quit();
    SDL_Quit();
}
