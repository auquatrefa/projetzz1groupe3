#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    SDL_Window *window = NULL;     // initialisation fenetre
    SDL_Renderer *renderer = NULL; // initialisation renderer

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n",
                SDL_GetError()); // l'initialisation de la SDL a échoué
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    window = SDL_CreateWindow("Animation", (mode.w - 1000) / 2, (mode.h - 496) / 2, 1000, 496,
                              SDL_WINDOW_RESIZABLE);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",
                SDL_GetError()); // la création de la fenêtre a échoué
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (renderer == NULL)
    {
        SDL_Log("Error : SDL renderer creation - %s\n",
                SDL_GetError()); // la création du renderer a échoué
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_bool program_on = SDL_TRUE, pause = SDL_FALSE;

    SDL_Texture *fond, *terre, *marche, *debout;

    fond = IMG_LoadTexture(renderer, "./textures/fond.png");
    if (fond == NULL)
    {
        SDL_Log("Error : SDL background load - %s\n",
                SDL_GetError()); // le chargement de l'image de fond a échoué
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    terre = IMG_LoadTexture(renderer, "./textures/terre.png");
    if (terre == NULL)
    {
        SDL_Log("Error : SDL tile load - %s\n",
                SDL_GetError()); // le chargement de la terre a échoué
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    marche = IMG_LoadTexture(renderer, "./textures/marche.png");
    if (marche == NULL)
    {
        SDL_Log("Error : SDL sprite load - %s\n",
                SDL_GetError()); // le chargement du sprite a échoué
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    debout = IMG_LoadTexture(renderer, "./textures/debout.png");
    if (debout == NULL)
    {
        SDL_Log("Error : SDL sprite load - %s\n",
                SDL_GetError()); // le chargement du sprite a échoué
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_Event event;

    SDL_Rect source_fond = {0}, window_dimensions = {0}, destination_fond = {0};
    SDL_Rect source_terre = {0}, destination_terre = {0};
    SDL_Rect source_marche = {0}, source_debout = {0}, destination_yeti = {0}, etat_yeti = {0};

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    SDL_QueryTexture(fond, NULL, NULL, &source_fond.w, &source_fond.h);
    SDL_QueryTexture(terre, NULL, NULL, &source_terre.w, &source_terre.h);
    SDL_QueryTexture(marche, NULL, NULL, &source_marche.w, &source_marche.h);
    SDL_QueryTexture(debout, NULL, NULL, &source_debout.w, &source_debout.h);

    float zoom_terre = 0.5;
    float zoom_yeti = 0.2;

    destination_fond = source_fond;
    destination_terre.w = source_terre.w * zoom_terre;
    destination_terre.h = source_terre.h * zoom_terre;
    destination_terre.x = 0;
    destination_terre.y = window_dimensions.h - source_terre.h * zoom_terre;

    destination_yeti.w = source_debout.w * zoom_yeti;
    destination_yeti.h = source_debout.h * zoom_yeti;
    destination_yeti.x = (window_dimensions.w - destination_yeti.h) / 2;
    destination_yeti.y = 275;

    etat_yeti.x = 0;
    etat_yeti.y = 0;
    etat_yeti.w = source_debout.w;
    etat_yeti.h = source_debout.h;

    while (program_on)
    {
        if (SDL_PollEvent(&event))
        { // si on a un evenement dans la file
            switch (event.type)
            {
            case SDL_QUIT:
                program_on = SDL_FALSE;
                break;
            case SDL_KEYDOWN:
                if (event.key.keysym.sym == SDLK_SPACE)
                {
                    pause = !pause;
                    break;
                }
            }
        }

        SDL_RenderClear(renderer);

        SDL_RenderCopy(renderer, fond, &source_fond, &destination_fond);
        destination_fond.x += 1000;
        SDL_RenderCopy(renderer, fond, &source_fond, &destination_fond);

        int base_terre = destination_terre.x;
        for (int i = 0; i < (destination_fond.w / destination_terre.w) + 2; ++i)
        {
            SDL_RenderCopy(renderer, terre, &source_terre, &destination_terre);
            destination_terre.x += destination_terre.w;
        }
        destination_terre.x = base_terre;

        if (pause)
        {
            SDL_RenderCopy(renderer, debout, &source_debout, &destination_yeti);
        } else {
            etat_yeti.x += source_debout.w;
            etat_yeti.x %= source_marche.w;
            SDL_RenderCopy(renderer, marche, &etat_yeti, &destination_yeti);
        }

        SDL_RenderPresent(renderer);
        SDL_Delay(100);

        if (destination_fond.x <= 0)
        {
            destination_fond.x += 1000;
        }

        if (pause)
        {
            destination_fond.x -= 1000;
        }
        else
        {
            destination_fond.x -= 1008;
            destination_terre.x -= 15;
        }

        if (destination_terre.x <= -destination_terre.w)
        {
            destination_terre.x += destination_terre.w;
        }
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}