#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "jeu.h"
#include "affichSDL.h"
#include "mcts.h"

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    srand(time(NULL));

    /* INITIALISATION */
    etat *mon_etat = initJeu();
    coup_p1 *mon_coup_p1 = NULL;
    coup_p2 *mon_coup_p2 = NULL;
    int tour = 1;
    // initSDL();

    /* BOUCLE DE JEU */
    while (!aFini(mon_etat, tour % 2))
    {
        afficheJeuTexte(mon_etat);
        printf("Défausse : %d\n", mon_etat->defausse);
        // afficheJeu(mon_etat);
        if (tour % 2)
        {
            // mon_coup_p2 = entrees(tour % 2, mon_etat);
            // if (mon_coup_p2 == NULL)
            // {
            //     finSDL(0, "On ferme la fenêtre");
            //     libereJeu(mon_etat);
            //     exit(EXIT_FAILURE);
            // }
            mon_coup_p1 = (coup_p1 *)malloc(sizeof(coup_p1));
            mon_coup_p1->joueur = tour % 2;
            printf("Piocher ? ");
            scanf("%d", &mon_coup_p1->type);
            appliqueP1(mon_etat, mon_coup_p1);
            mon_coup_p2 = (coup_p2 *)malloc(sizeof(coup_p2));
            mon_coup_p2->coord_carte = (int *)malloc(2 * sizeof(int));
            mon_coup_p2->joueur = tour % 2;
            if (mon_coup_p1->type)
            {
                mon_coup_p2->carte_entree = mon_etat->carte_piochee;
                printf("Carte piochée : %d\n", mon_etat->carte_piochee);
            }
            else
            {
                mon_coup_p2->carte_entree = mon_etat->defausse;
            }
            printf("Coup : ");
            scanf("%d", &mon_coup_p2->type);
            printf("X : ");
            scanf("%d", &mon_coup_p2->coord_carte[0]);
            printf("Y : ");
            scanf("%d", &mon_coup_p2->coord_carte[1]);
            free(mon_coup_p1);
        }
        else
        {
            mon_coup_p2 = choisirCoup(mon_etat, tour % 2);
            printf("%d %d %d %d\n", mon_coup_p2->type, mon_coup_p2->carte_entree, mon_coup_p2->coord_carte[0], mon_coup_p2->coord_carte[1]);
        }

        appliqueP2(mon_etat, mon_coup_p2);
        tour++;
    }

    decouvreJeu(mon_etat, !(tour % 2));
    afficheJeuTexte(mon_etat);
    // afficheJeu(mon_etat);
    printf("Score joueur 1 : %d\n", calculeScore(mon_etat, 0));
    printf("Score joueur 2 : %d\n", calculeScore(mon_etat, 1));
    //sleep(10);
    // afficheFin(calculeScore(mon_etat, 0), calculeScore(mon_etat, 1));
    libereJeu(mon_etat);
    // finSDL(0, "FIN");
    return 0;
}