#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "affichSDL.h"
#include "jeu.h"

/* VARIABLES GLOBALES */
SDL_Texture *ma_texture; // Texture contenant les cartes à afficher
donnees_f_t ma_fenetre;  // Structure contenant les informations sur la fenêtre
SDL_DisplayMode mode;
SDL_Texture *moinsUn;
SDL_Texture *moinsDeux;
SDL_Texture *zero;
SDL_Texture *un;
SDL_Texture *deux;
SDL_Texture *trois;
SDL_Texture *quatre;
SDL_Texture *cinq;
SDL_Texture *six;
SDL_Texture *sept;
SDL_Texture *huit;
SDL_Texture *neuf;
SDL_Texture *dix;
SDL_Texture *onze;
SDL_Texture *douze;
SDL_Texture *dos;
SDL_Texture *fondEcran;
TTF_Font *font = NULL;
TTF_Font *font1 = NULL;

/*****************************************************************************************/
/* Fonction finSDL                                                                      */
/*                                                                                       */
/* Cette fonction permet de quitter la SDL en cas d'erreur                               */
/*                                                                                       */
/* Paramètres :                                                                          */
/*      - char ok : 0 si tout s'est bien passé, 1 sinon                                  */
/*      - char const *msg : message à afficher en cas d'erreur                           */
/*      - SDL_Window *window : fenêtre à fermer                                          */
/*      - SDL_Renderer *renderer : renderer à fermer                                     */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void finSDL(char ok, // fin normale : ok = 0 ; anormale ok = 1
            char const *msg)
{ // message à afficher
    char msg_formate[255];
    int l;

    if (ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formate, msg, 250);
        l = strlen(msg_formate);
        strcpy(msg_formate + l, " : %s\n");

        SDL_Log(msg_formate, SDL_GetError());
    }

    if (ma_fenetre.rendu != NULL)
    {                                          // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(ma_fenetre.rendu); // Attention : on suppose que les NULL sont maintenus !!
        ma_fenetre.rendu = NULL;
    }
    if (ma_fenetre.fenetre != NULL)
    {                                          // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(ma_fenetre.fenetre); // Attention : on suppose que les NULL sont maintenus !!
        ma_fenetre.fenetre = NULL;
    }

    TTF_Quit();
    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/*****************************************************************************************/
/* Fonction initSDL                                                                      */
/*                                                                                       */
/* Cette fonction initialise la structure ma_fenetre permettant de modéliser la fenêtre  */
/* de jeu                                                                                */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void initSDL()
{
    ma_fenetre.fenetre = NULL;
    ma_fenetre.rendu = NULL;

    /* Création de la SDL */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        finSDL(1, "ERROR SDL INIT");

    SDL_GetCurrentDisplayMode(0, &mode); // données de l'ecran

    /* Création de la fenêtre */
    ma_fenetre.fenetre = SDL_CreateWindow("SKYJO", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1025, 1025, SDL_WINDOW_OPENGL);
    if (ma_fenetre.fenetre == NULL)
        finSDL(1, "ERROR WINDOW CREATION");

    /* Création du renderer */
    ma_fenetre.rendu = SDL_CreateRenderer(ma_fenetre.fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (ma_fenetre.rendu == NULL)
        finSDL(1, "ERROR RENDERER CREATION");

    /* Creation de la font */
    if (TTF_Init() < 0)
        finSDL(1, "Couldn't initialize SDL TTF");
    font = TTF_OpenFont("./fonts/chlorinar.regular.ttf", 90); // La police à charger, la taille désirée
    if (font == NULL)
        finSDL(1, "Can't load font");

    /* Creation de la font1 */
    if (TTF_Init() < 0)
        finSDL(1, "Couldn't initialize SDL TTF");
    font1 = TTF_OpenFont("./fonts/fake.receipt.ttf", 50); // La police à charger, la taille désirée
    if (font1 == NULL)
        finSDL(1, "Can't load font");

    initTextures();
}

/*****************************************************************************************/
/* Fonction initTextures                                                                 */
/*                                                                                       */
/* Cette fonction initialise les textures qui représenteront les cartes                  */
/*                                                                                       */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void initTextures()
{
    /* Création des textures */
    fondEcran = IMG_LoadTexture(ma_fenetre.rendu, "./images/fond.png");
    if (fondEcran == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    moinsUn = IMG_LoadTexture(ma_fenetre.rendu, "./images/moins1.png");
    if (moinsUn == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    moinsDeux = IMG_LoadTexture(ma_fenetre.rendu, "./images/moins2.png");
    if (moinsDeux == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    zero = IMG_LoadTexture(ma_fenetre.rendu, "./images/zero.png");
    if (zero == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    un = IMG_LoadTexture(ma_fenetre.rendu, "./images/un.png");
    if (un == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    deux = IMG_LoadTexture(ma_fenetre.rendu, "./images/deux.png");
    if (deux == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    trois = IMG_LoadTexture(ma_fenetre.rendu, "./images/trois.png");
    if (trois == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    quatre = IMG_LoadTexture(ma_fenetre.rendu, "./images/quatre.png");
    if (quatre == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    cinq = IMG_LoadTexture(ma_fenetre.rendu, "./images/cinq.png");
    if (cinq == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    six = IMG_LoadTexture(ma_fenetre.rendu, "./images/six.png");
    if (six == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    sept = IMG_LoadTexture(ma_fenetre.rendu, "./images/sept.png");
    if (sept == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    huit = IMG_LoadTexture(ma_fenetre.rendu, "./images/huit.png");
    if (huit == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    neuf = IMG_LoadTexture(ma_fenetre.rendu, "./images/neuf.png");
    if (neuf == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    dix = IMG_LoadTexture(ma_fenetre.rendu, "./images/dix.png");
    if (dix == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    onze = IMG_LoadTexture(ma_fenetre.rendu, "./images/onze.png");
    if (onze == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    douze = IMG_LoadTexture(ma_fenetre.rendu, "./images/douze.png");
    if (douze == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
    dos = IMG_LoadTexture(ma_fenetre.rendu, "./images/dos.png");
    if (dos == NULL)
        finSDL(1, "Echec du chargement de l'image dans la texture");
}

/*****************************************************************************************/
/* Fonction entrees                                                                      */
/*                                                                                       */
/* Cette fonction permet de quitter la SDL en cas d'erreur                               */
/*                                                                                       */
/* Paramètre :                                                                           */
/*      - int joueur : donne le joueur en train de jouer                                 */
/*                                                                                       */
/* Retour : struct coup donnant le coup joué par qui et la carte en question             */
/*                                                                                       */
/*****************************************************************************************/
/* FINIR TRAITER TOUS LES CAS EN FONCTIONS DES COORDONNEES QU'ON AURA */
coup_p2 *entrees(int joueur, etat *mon_etat)
{
    (void)joueur;
    SDL_Event evenement;
    SDL_bool event_utile = SDL_FALSE;
    int x, y;
    coup_p1 *premiere_phase = NULL;
    coup_p2 *deuxieme_phase = NULL;
    int clique_sur_pioche = 0;
    int clique_sur_defausse = 0;
    char str[255];
    if (joueur == 0)
    {
        sprintf(str, "Ordi");
    }
    else
    {
        sprintf(str, "A vous de jouer");
    }
    //printf("%s\n", str);

    /* Affichage du texte */
    SDL_Color color = {255, 255, 0, 255};                    // la couleur du texte
    SDL_Surface *text_surface = NULL;                        // la surface  (uniquement transitoire)
    text_surface = TTF_RenderText_Blended(font, str, color); // création du texte dans la surface
    if (text_surface == NULL)
        finSDL(1, "Can't create text surface");

    SDL_Texture *text_texture = NULL;                                            // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(ma_fenetre.rendu, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL)
        finSDL(1, "Can't create texture from surface");
    SDL_FreeSurface(text_surface); // la texture ne sert plus à rien

    SDL_Rect pos = {50, 898, 258, 64};                          // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h); // récupération de la taille (w, h) du texte
    SDL_RenderCopy(ma_fenetre.rendu, text_texture, NULL, &pos); // Ecriture du texte dans le renderer
    SDL_DestroyTexture(text_texture);                           // On n'a plus besoin de la texture avec le texte

    SDL_RenderPresent(ma_fenetre.rendu);

    // On attend tant que le joueur clique et que ça soit une action valide
    while (!event_utile)
    {
        if (SDL_PollEvent(&evenement))
        {
            switch (evenement.type)
            {
            case SDL_WINDOWEVENT:
                switch (evenement.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    puts("Appui sur la croix");
                    // A traiter dans le main: si le retour est null alors quitter
                    return NULL;
                    break;
                default:
                    break;
                }
                break;
            case SDL_QUIT:
                puts("On quitte");
                // A traiter dans le main: si le retour est null alors quitter
                return NULL;
                break;
            case SDL_MOUSEBUTTONDOWN:
                //puts("Test1");
                x = evenement.motion.x;
                y = evenement.motion.y;
                SDL_Rect destination = {0};
                destination.w = 75;
                destination.h = 115;

                /* Implémentation Première Phase */
                if (!clique_sur_pioche && x > 128 && x < 256 && y > 128 && y < 241)
                {
                    /* click pioche */
                    //puts("Test");
                    clique_sur_pioche = 1;

                    premiere_phase = (coup_p1 *)malloc(sizeof(coup_p1));
                    premiere_phase->joueur = joueur;
                    premiere_phase->type = 1;

                    appliqueP1(mon_etat, premiere_phase);

                    deuxieme_phase = (coup_p2 *)malloc(sizeof(coup_p2));
                    deuxieme_phase->coord_carte = (int *)malloc(2 * sizeof(int));
                    deuxieme_phase->joueur = joueur;
                    deuxieme_phase->type = 0;
                    deuxieme_phase->carte_entree = mon_etat->carte_piochee;

                    destination.x = 128;
                    destination.y = 768;
                    affichageCarte(deuxieme_phase->carte_entree, &destination);
                    SDL_RenderPresent(ma_fenetre.rendu);
                }

                if (!clique_sur_defausse && x > 128 && x < 256 && y > 384 && y < 499)
                {
                    /* click défausse */
                    if (clique_sur_pioche)
                    {
                        deuxieme_phase->type = 1;
                    }
                    else
                    {
                        premiere_phase = (coup_p1 *)malloc(sizeof(coup_p1));
                        premiere_phase->joueur = joueur;
                        premiere_phase->type = 0;

                        appliqueP1(mon_etat, premiere_phase);

                        deuxieme_phase = (coup_p2 *)malloc(sizeof(coup_p2));
                        deuxieme_phase->coord_carte = (int *)malloc(2 * sizeof(int));
                        deuxieme_phase->joueur = joueur;
                        deuxieme_phase->type = 0;
                        deuxieme_phase->carte_entree = mon_etat->defausse;

                        free(premiere_phase);
                    }
                }

                /* Implémentation Deuxième Phase */
                if (y > (64 + joueur * 514) && y < (179 + joueur * 514))
                {
                    /* Première ligne */
                    if (x > 512 && x < 587)
                    {
                        /* Première colonne */
                        return remplie_deuxieme_phase(deuxieme_phase, 0, 0, joueur, mon_etat);
                    }
                    else
                    {
                        if (x > 615 && x < 690)
                        {
                            /* Deuxième colonne */
                            return remplie_deuxieme_phase(deuxieme_phase, 0, 1, joueur, mon_etat);
                        }
                        else
                        {
                            if (x > 718 && x < 793)
                            {
                                /* Troisième colonne */
                                return remplie_deuxieme_phase(deuxieme_phase, 0, 2, joueur, mon_etat);
                            }
                            else
                            {
                                if (x > 821 && x < 896)
                                {
                                    /* Quatrième colonne */
                                    return remplie_deuxieme_phase(deuxieme_phase, 0, 3, joueur, mon_etat);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (y > (207 + joueur * 514) && y < (322 + joueur * 514))
                    {
                        /* Deuxième ligne */
                        if (x > 512 && x < 587)
                        {
                            /* Première colonne */
                            return remplie_deuxieme_phase(deuxieme_phase, 1, 0, joueur, mon_etat);
                        }
                        else
                        {
                            if (x > 615 && x < 690)
                            {
                                /* Deuxième colonne */
                                return remplie_deuxieme_phase(deuxieme_phase, 1, 1, joueur, mon_etat);
                            }
                            else
                            {
                                if (x > 718 && x < 793)
                                {
                                    /* Troisième colonne */
                                    return remplie_deuxieme_phase(deuxieme_phase, 1, 2, joueur, mon_etat);
                                }
                                else
                                {
                                    if (x > 821 && x < 896)
                                    {
                                        /* Quatrième colonne */
                                        return remplie_deuxieme_phase(deuxieme_phase, 1, 3, joueur, mon_etat);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (y > (350 + joueur * 514) && y < (465 + joueur * 514))
                        {
                            /* Troisième ligne */
                            if (x > 512 && x < 587)
                            {
                                /* Première colonne */
                                return remplie_deuxieme_phase(deuxieme_phase, 2, 0, joueur, mon_etat);
                            }
                            else
                            {
                                if (x > 615 && x < 690)
                                {
                                    /* Deuxième colonne */
                                    return remplie_deuxieme_phase(deuxieme_phase, 2, 1, joueur, mon_etat);
                                }
                                else
                                {
                                    if (x > 718 && x < 793)
                                    {
                                        /* Troisième colonne */
                                        return remplie_deuxieme_phase(deuxieme_phase, 2, 2, joueur, mon_etat);
                                    }
                                    else
                                    {
                                        if (x > 821 && x < 896)
                                        {
                                            /* Quatrième colonne */
                                            return remplie_deuxieme_phase(deuxieme_phase, 2, 3, joueur, mon_etat);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                break;

            default:
                // attente d'instruction
                break;
            }
        }
    }
    return deuxieme_phase;
}

/*****************************************************************************************/
/* Fonction remplie_deuxième_phase                                                       */
/*                                                                                       */
/* Cette fonction permet de remplir les données de la deuxième phase en fonction du coup */
/*                                                                                       */
/* Paramètre :                                                                           */
/*      - coup_p2 *deuxieme_phase : pointeur à renvoyé rempli avec les bonnes valeurs    */
/*      - int i : coordonnée de la carte dans le tableau représentant la main du joueur  */
/*      - int j : de même                                                                */
/*      - int joueur : indique le joueur dont c'est le tour                              */
/*      - etat *mon_etat : pointeur nous donnant les informations sur le type de coup    */
/*                                                                                       */
/* Retour : struct coup donnant le coup joué par qui et la carte en question             */
/*                                                                                       */
/*****************************************************************************************/
coup_p2 *remplie_deuxieme_phase(coup_p2 *deuxieme_phase, int i, int j, int joueur, etat *mon_etat)
{
    if (!deuxieme_phase )
    {
        deuxieme_phase = (coup_p2 *)malloc(sizeof(coup_p2));
        deuxieme_phase->coord_carte = (int *)malloc(2 * sizeof(int));
        deuxieme_phase->carte_entree = mon_etat->defausse;
        deuxieme_phase->joueur = joueur;
        deuxieme_phase->type = 1;
        deuxieme_phase->coord_carte[0] = i;
        deuxieme_phase->coord_carte[1] = j;
        return deuxieme_phase;
    }
    else
    {
        deuxieme_phase->coord_carte[0] = i;
        deuxieme_phase->coord_carte[1] = j;
        return deuxieme_phase;
    }
}

/*****************************************************************************************/
/* Fonction affichageCarte                                                               */
/*                                                                                       */
/* Cette fonction permet d'aficher la carte donnée en paramètres                         */
/*                                                                                       */
/* Paramètre :                                                                           */
/*      - int valeur : valeur de la carte à afficher                                     */
/*      - SDL_Rect *destination : rectangle représentant la zone où la carte est affichée*/
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void affichageCarte(int valeur, SDL_Rect *destination)
{
    switch (valeur)
    {
    case -2:
        SDL_RenderCopy(ma_fenetre.rendu, moinsDeux, NULL, destination);
        break;
    case -1:
        SDL_RenderCopy(ma_fenetre.rendu, moinsUn, NULL, destination);
        break;
    case 0:
        SDL_RenderCopy(ma_fenetre.rendu, zero, NULL, destination);
        break;
    case 1:
        SDL_RenderCopy(ma_fenetre.rendu, un, NULL, destination);
        break;
    case 2:
        SDL_RenderCopy(ma_fenetre.rendu, deux, NULL, destination);
        break;
    case 3:
        SDL_RenderCopy(ma_fenetre.rendu, trois, NULL, destination);
        break;
    case 4:
        SDL_RenderCopy(ma_fenetre.rendu, quatre, NULL, destination);
        break;
    case 5:
        SDL_RenderCopy(ma_fenetre.rendu, cinq, NULL, destination);
        break;
    case 6:
        SDL_RenderCopy(ma_fenetre.rendu, six, NULL, destination);
        break;
    case 7:
        SDL_RenderCopy(ma_fenetre.rendu, sept, NULL, destination);
        break;
    case 8:
        SDL_RenderCopy(ma_fenetre.rendu, huit, NULL, destination);
        break;
    case 9:
        SDL_RenderCopy(ma_fenetre.rendu, neuf, NULL, destination);
        break;
    case 10:
        SDL_RenderCopy(ma_fenetre.rendu, dix, NULL, destination);
        break;
    case 11:
        SDL_RenderCopy(ma_fenetre.rendu, onze, NULL, destination);
        break;
    case 12:
        SDL_RenderCopy(ma_fenetre.rendu, douze, NULL, destination);
        break;
    case -3:
        // Carte absente (quand une col a été supr par ex)
        break;
    default:
        puts("On ne devrait pas être là");
        exit(-5);
        break;
    }
}

/*****************************************************************************************/
/* Fonction afficheJeu                                                                   */
/*                                                                                       */
/* Cette fonction permet d'afficher l'ensemble du jeu                                    */
/*                                                                                       */
/* Paramètre :                                                                           */
/*      - etat *mon_etat : donne les infos relatives au jeu à afficher                   */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void afficheJeu(etat *mon_etat)
{

    /* Préparation du rendu, de la fenêtre, des textures... */
    SDL_RenderClear(ma_fenetre.rendu);

    /* Affichage fond */
    SDL_RenderCopy(ma_fenetre.rendu, fondEcran, NULL, NULL);

    /* Rectangle accueillant les textures des cartes */
    SDL_Rect destination = {0};
    destination.w = 75;
    destination.h = 115;

    /* Parcourt de la main des joueurs pour affciher les cartes */
    for (int j = 0; j < 2; ++j)
    {
        for (int l = 0; l < 3; ++l)
        {
            for (int c = 0; c < 4; ++c)
            {
                carte actuel = mon_etat->mains[j][l][c];
                destination.x = 512 + c * (75 + 28);
                destination.y = 64 + j * 513 + l * (115 + 19);
                if (actuel.est_retournee)
                {
                    affichageCarte(actuel.valeur, &destination);
                }
                else
                {
                    SDL_RenderCopy(ma_fenetre.rendu, dos, NULL, &destination);
                }
            }
        }
    }

    /* Affichage de la pioche */
    destination.x = 128;
    destination.y = 128;
    SDL_RenderCopy(ma_fenetre.rendu, dos, NULL, &destination);

    /* Affichage de la defausse */
    destination.x = 128;
    destination.y = 384;
    affichageCarte(mon_etat->defausse, &destination);

    SDL_RenderPresent(ma_fenetre.rendu);
}

void afficheFin(int score1, int score2)
{
    /* Préparation du rendu, de la fenêtre, des textures... */
    SDL_RenderClear(ma_fenetre.rendu);

    /* Affichage fond */
    SDL_RenderCopy(ma_fenetre.rendu, fondEcran, NULL, NULL);

    (void)score1;
    (void)score2;

    char str1[255];
    char str2[255];
    char str3[255];
    sprintf(str1,"FIN");
    sprintf(str2,"Votre score %d",score1);
    sprintf(str3,"Score ordinateur %d", score2);

    SDL_Color color = {255, 255, 0, 255}; // la couleur du texte
    SDL_Surface *text_surface = NULL;     // la surface  (uniquement transitoire)

    /* Affichage du texte 1*/
    text_surface = TTF_RenderText_Blended(font1, str1, color); // création du texte dans la surface
    if (text_surface == NULL)
        finSDL(1, "Can't create text surface");

    SDL_Texture *text_texture = NULL;                                            // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(ma_fenetre.rendu, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL)
        finSDL(1, "Can't create texture from surface");
    //SDL_FreeSurface(text_surface); // la texture ne sert plus à rien

    SDL_Rect pos = {480, 125, 10, 64};                           // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos.w, &pos.h); // récupération de la taille (w, h) du texte
    SDL_RenderCopy(ma_fenetre.rendu, text_texture, NULL, &pos); // Ecriture du texte dans le renderer
    //SDL_DestroyTexture(text_texture);                           // On n'a plus besoin de la texture avec le texte

    //SDL_RenderPresent(ma_fenetre.rendu);

    /* Affichage du texte 2*/
    text_surface = TTF_RenderText_Blended(font1, str2, color); // création du texte dans la surface
    if (text_surface == NULL)
        finSDL(1, "Can't create text surface");

    text_texture = NULL;                                                         // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(ma_fenetre.rendu, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL)
        finSDL(1, "Can't create texture from surface");
    //SDL_FreeSurface(text_surface); // la texture ne sert plus à rien

    SDL_Rect pos2 = {100, 400, 10, 15};                           // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos2.w, &pos2.h); // récupération de la taille (w, h) du texte
    SDL_RenderCopy(ma_fenetre.rendu, text_texture, NULL, &pos2); // Ecriture du texte dans le renderer
    //SDL_DestroyTexture(text_texture);                           // On n'a plus besoin de la texture avec le texte

    //SDL_RenderPresent(ma_fenetre.rendu);

    /* Affichage du texte 3*/
    text_surface = TTF_RenderText_Blended(font1, str3, color); // création du texte dans la surface
    if (text_surface == NULL)
        finSDL(1, "Can't create text surface");

    text_texture = NULL;                                                         // la texture qui contient le texte
    text_texture = SDL_CreateTextureFromSurface(ma_fenetre.rendu, text_surface); // transfert de la surface à la texture
    if (text_texture == NULL)
        finSDL(1, "Can't create texture from surface");
    //SDL_FreeSurface(text_surface); // la texture ne sert plus à rien

    SDL_Rect pos3 = {100, 500, 10, 15};                           // rectangle où le texte va être prositionné
    SDL_QueryTexture(text_texture, NULL, NULL, &pos3.w, &pos3.h); // récupération de la taille (w, h) du texte
    SDL_RenderCopy(ma_fenetre.rendu, text_texture, NULL, &pos3); // Ecriture du texte dans le renderer
    //SDL_DestroyTexture(text_texture);                           // On n'a plus besoin de la texture avec le texte

    SDL_RenderPresent(ma_fenetre.rendu);

    SDL_Delay(4000);
}