#ifndef affichSDL_h
#define affichSDL_h

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "jeu.h"

typedef struct donnees_fenetre_s
{
    SDL_Window *fenetre;
    SDL_Renderer *rendu;
    int largeur;
    int hauteur;
} donnees_f_t;

void initSDL();
void initTextures();
void finSDL(char ok, char const *msg);
coup_p2* entrees(int joueur, etat *mon_etat);
void afficheJeu(etat *mon_etat);
void affichageCarte(int valeur, SDL_Rect *destination);
coup_p2* remplie_deuxieme_phase(coup_p2 *deuxieme_phase, int i, int j, int joueur, etat *mon_etat);
void afficheFin(int score1, int score2);
#endif