#ifndef jeu_h
#define jeu_h

#include <stdio.h>
#include <stdlib.h>

typedef struct carte
{
    int est_retournee;
    int valeur;
} carte;

typedef struct carte_pioche
{
    int valeur;
    struct carte_pioche *suiv;
} carte_pioche;

typedef struct pioche
{
    carte_pioche *carte_deb;
    carte_pioche *carte_fin;
} pioche;

typedef struct etat
{
    carte mains[2][3][4];
    int defausse;
    int carte_piochee;
    pioche *pioche;
} etat;

typedef struct coup_p1
{
    int type; // 0 : pas pioche, 1 : pioche
    int joueur;
} coup_p1;

typedef struct coup_p2
{
    int type; // 0 : echange, 1 : retourne
    int joueur;
    int carte_entree;
    int *coord_carte;
} coup_p2;

int *initTabPioche();
int *melangeTabPioche(int *tab);
int piocheCarte(etat *mon_etat);
int tetePioche(etat *mon_etat);
void ajoutePioche(etat *mon_etat, int carte);
void actualiseDefausse(etat *mon_etat, int carte);
etat *initJeu();
void retourneCarte(etat *mon_etat, int joueur, int *coord_carte);
void echangeCarte(etat *mon_etat, int joueur, int *coord_carte, int carte);
void verifColonne(etat *mon_etat, int joueur);
void appliqueP1(etat *mon_etat, coup_p1 *mon_coup);
void appliqueP2(etat *mon_etat, coup_p2 *mon_coup);
etat *copieJeu(etat *mon_etat);
int aFini(etat *mon_etat, int joueur);
void decouvreJeu(etat *mon_etat, int joueur);
int calculeScore(etat *mon_etat, int joueur);
void afficheJeuTexte(etat *mon_etat);
void liberePioche(pioche *ma_pioche);
void libereJeu(etat *mon_etat);

#endif