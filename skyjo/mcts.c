#include <time.h>
#include <math.h>

#include "mcts.h"

int prof;
noeud_rn *arbre;

/****************************************************/
/* piocheProba()                                    */
/*                                                  */
/* Retourne la carte piochée en fonction de la      */
/* probabilité de piocher une carte.                */
/*                                                  */
/* Entrées :                                        */
/* - void                                           */
/*                                                  */
/* Sortie :                                         */
/* - int : carte piochée                            */
/****************************************************/
int piocheProba()
{
    int k = rand() % 150; // On tire un nombre aléatoire entre 0 et 149

    if (k < 5) // k compris entre 0 et 4
    {
        return -2;
    }
    else if (k < 15) // k compris entre 5 et 14
    {
        return -1;
    }
    else if (k < 30) // k compris entre 15 et 29
    {
        return 0;
    }
    else if (k < 40) // k compris entre 30 et 39
    {
        return 1;
    }
    else if (k < 50) // k compris entre 40 et 49
    {
        return 2;
    }
    else if (k < 60) // k compris entre 50 et 59
    {
        return 3;
    }
    else if (k < 70) // k compris entre 60 et 69
    {
        return 4;
    }
    else if (k < 80) // k compris entre 70 et 79
    {
        return 5;
    }
    else if (k < 90) // k compris entre 80 et 89
    {
        return 6;
    }
    else if (k < 100) // k compris entre 90 et 99
    {
        return 7;
    }
    else if (k < 110) // k compris entre 100 et 109
    {
        return 8;
    }
    else if (k < 120) // k compris entre 110 et 119
    {
        return 9;
    }
    else if (k < 130) // k compris entre 120 et 129
    {
        return 10;
    }
    else if (k < 140) // k compris entre 130 et 139
    {
        return 11;
    }
    else // k compris entre 140 et 149
    {
        return 12;
    }
}

/****************************************************/
/* genereComplementaires()                          */
/*                                                  */
/* Génère les cartes complémentaires pour un joueur */
/* (pioche + cartes face-cachée)                    */
/*                                                  */
/* Entrées :                                        */
/* - etat *mon_etat : état du jeu                   */
/* - vue *ma_vue : vue du joueur                    */
/* - int joueur : joueur concerné                   */
/*                                                  */
/* Sortie :                                         */
/* - void                                           */
/****************************************************/
void genereComplementaires(etat *mon_etat, vue *ma_vue, int joueur)
{
    /* On change la premiere carte de la pioche avec une carte pioché en proba */
    mon_etat->pioche->carte_deb->valeur = piocheProba();

    /* Pour chaque carte face-cachée de la vue du joueur, on donne une valeur aléatoire */
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            /* Si la carte n'est pas visible par le joueur */
            if (ma_vue->ma_main[i][j] == -3)
            {
                /* On assigne une valeur pour la carte */
                mon_etat->mains[joueur][i][j].valeur = piocheProba();
            }
        }
    }
}

/****************************************************/
/* UCB()                                            */
/*                                                  */
/* Calcule l'UCP d'un coup.                         */
/*                                                  */
/* Entrées :                                        */
/* - liste_coups *coup : coup à évaluer             */
/*                                                  */
/* Sortie :                                         */
/* - float : UCB du coup                            */
/****************************************************/
float UCB(liste_coups *coup, int N)
{
    float gain_moyen = coup->score / coup->n_exec;
    float c = sqrt(2);

    return gain_moyen + c * sqrt(log(N) / coup->n_exec);
}

/****************************************************/
/* selectionne()                                    */
/*                                                  */
/* Sélectionne le meilleur coup à jouer parmi une   */
/* liste de coups.                                  */
/*                                                  */
/* Entrées :                                        */
/* - liste_coups *ma_liste_coups : liste des coups  */
/*                                                  */
/* Sortie :                                         */
/* - liste_coups * : meilleur coup à jouer          */
/****************************************************/
liste_coups *selectionne(noeud *mon_noeud)
{
    /* Creation du meilleur coup actuel (= premier coup au debut) */
    liste_coups *meilleur_coup = mon_noeud->coups;

    /* Si le premier coup n'a jamais été éxécuté */
    if (meilleur_coup->n_exec == 0)
    {
        return meilleur_coup;
    }

    /* On initialise le meilleur score avec le score du premier coup */
    float meilleur_ucb = UCB(meilleur_coup, mon_noeud->n_parties);

    /* On parcourt la liste des coups */
    liste_coups *cour = mon_noeud->coups->suiv;
    while (cour != NULL)
    {
        /* Si le coup n'a jamais été éxécuté */
        if (cour->n_exec == 0)
        {
            return cour;
        }

        /* On calcule l'UCB du coup courant */
        float ucb_cour = UCB(cour, mon_noeud->n_parties);

        /* Si l'UCB du coup courant est supérieur à l'UCB du meilleur coup */
        if (ucb_cour > meilleur_ucb)
        {
            /* On met à jour le meilleur coup */
            meilleur_coup = cour;
            meilleur_ucb = ucb_cour;
        }

        /* On passe au coup suivant */
        cour = cour->suiv;
    }

    return meilleur_coup;
}

/*****************************************************************************************/
/* Fonction genereVue                                                                    */
/*                                                                                       */
/* Cette fonction permet de générer la vue en fonction de l'état dans lequel on se trouve*/
/*                                                                                       */
/* Paramètres :                                                                          */
/*      - int valeur : valeur de la carte à afficher                                     */
/*      - SDL_Rect *destination : rectangle représentant la zone où la carte est affichée*/
/*                                                                                       */
/* Retour : la vue initialisée correctement ou NULL si prblm                             */
/*                                                                                       */
/*****************************************************************************************/
vue *genereVue(etat *mon_etat, int joueur, int phase)
{
    vue *ma_vue = (vue *)malloc(sizeof(vue));
    if (ma_vue)
    {
        ma_vue->defausse = mon_etat->defausse;
        ma_vue->carte_piochee = mon_etat->carte_piochee;
        ma_vue->phase = phase;
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                if (mon_etat->mains[joueur][i][j].est_retournee)
                {
                    ma_vue->ma_main[i][j] = mon_etat->mains[joueur][i][j].valeur;
                }
                else
                {
                    ma_vue->ma_main[i][j] = -3;
                }
            }
        }
        return ma_vue;
    }
    else
    {
        return NULL;
    }
}

/*****************************************************************************************/
/* Fonction genereCoups                                                                  */
/*                                                                                       */
/* Cette fonction permet de générer les différents coups possibles pour l'algorithme MCTS*/
/*                                                                                       */
/* Paramètres :                                                                          */
/*      - noeud *mon_noeudp1 : noeud à partir du quel on génère les coups possibles        */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void genereCoups(noeud *mon_noeud)
{
    liste_coups *cour = NULL;
    if (mon_noeud->vue->phase == 1)
    {
        for (int i = 0; i < 2; ++i)
        {
            liste_coups *nouv = (liste_coups *)malloc(sizeof(liste_coups));
            nouv->score = 0;
            nouv->n_exec = 0;
            nouv->suiv = cour;
            nouv->coup_p1 = (coup_p1 *)malloc(sizeof(coup_p1));
            nouv->coup_p1->type = i;
            cour = nouv;
        }
    }
    else
    {
        for (int i = 0; i < 2; ++i)
        {
            for (int l = 0; l < 3; ++l)
            {
                for (int c = 0; c < 4; ++c)
                {
                    if (!i || mon_noeud->vue->ma_main[l][c] == -3)
                    {
                        liste_coups *nouv = (liste_coups *)malloc(sizeof(liste_coups));
                        nouv->score = 0;
                        nouv->n_exec = 0;
                        nouv->suiv = cour;
                        nouv->coup_p2 = (coup_p2 *)malloc(sizeof(coup_p2));
                        nouv->coup_p2->coord_carte = (int *)malloc(2 * sizeof(int));
                        nouv->coup_p2->type = i;
                        if (mon_noeud->vue->carte_piochee == -3)
                        {
                            nouv->coup_p2->carte_entree = mon_noeud->vue->defausse;
                        }
                        else
                        {
                            nouv->coup_p2->carte_entree = mon_noeud->vue->carte_piochee;
                        }
                        nouv->coup_p2->coord_carte[0] = l;
                        nouv->coup_p2->coord_carte[1] = c;
                        cour = nouv;
                    }
                }
            }
        }
    }

    mon_noeud->coups = cour;
}

float simule(etat *mon_etat, liste_coups *coup, int phase, int joueur)
{
    if (aFini(mon_etat, joueur))
    {
        decouvreJeu(mon_etat, !joueur);
        int score_joueur = 0, score_ennemi = 0;
        score_joueur = calculeScore(mon_etat, joueur);
        score_ennemi = calculeScore(mon_etat, !joueur);
        if (score_ennemi - score_joueur > 20)
        {
            return 1;
        }
        else if (score_ennemi - score_joueur < -20)
        {
            return 0;
        }
        else
        {
            return ((float)score_ennemi - (float)score_joueur + 20) / 40;
        }
    }
    else
    {
        if (phase == 1)
        {
            coup->coup_p1->joueur = joueur;
            appliqueP1(mon_etat, coup->coup_p1);
            liste_coups *coup_aleatoire = (liste_coups *)malloc(sizeof(liste_coups));
            coup_aleatoire->coup_p2 = (coup_p2 *)malloc(sizeof(coup_p2));
            coup_aleatoire->coup_p2->type = rand() % 2;
            coup_aleatoire->coup_p2->joueur = joueur;
            if (coup->coup_p1->type)
            {
                coup_aleatoire->coup_p2->carte_entree = mon_etat->carte_piochee;
            }
            else
            {
                coup_aleatoire->coup_p2->carte_entree = mon_etat->defausse;
            }

            if (coup_aleatoire->coup_p2->type)
            {
                coup_aleatoire->coup_p2->coord_carte = genereCarteNonRetourneeAlea(mon_etat, joueur);
            }
            else
            {
                coup_aleatoire->coup_p2->coord_carte = (int *)malloc(2 * sizeof(int));
                coup_aleatoire->coup_p2->coord_carte[0] = rand() % 3;
                coup_aleatoire->coup_p2->coord_carte[1] = rand() % 4;
            }
            return simule(mon_etat, coup_aleatoire, 2, joueur);
        }
        else
        {
            coup->coup_p1->joueur = joueur;
            appliqueP2(mon_etat, coup->coup_p2);
            liste_coups *coup_aleatoire = (liste_coups *)malloc(sizeof(liste_coups));
            coup_aleatoire->coup_p1 = (coup_p1 *)malloc(sizeof(coup_p1));
            coup_aleatoire->coup_p1->type = rand() % 2;
            coup_aleatoire->coup_p1->joueur = !joueur;
            return 1 - simule(mon_etat, coup_aleatoire, 1, !joueur);
        }
    }
}

float developpe(etat *mon_etat, int joueur, int phase)
{
    prof++;
    vue *ma_vue = genereVue(mon_etat, joueur, phase);
    noeud_rn *rech = rechercheNoeudRN(arbre, ma_vue);
    liste_coups *mon_coup = NULL;
    // printf("%d\n", prof);
    if (prof > 20)
    {
        return 0.5;
    }
    if (rech == NULL)
    {
        noeud *mon_noeud = (noeud *)malloc(sizeof(noeud));
        mon_noeud->vue = ma_vue;
        genereCoups(mon_noeud);
        arbre = insereNoeudRN(arbre, mon_noeud);
        mon_coup = selectionne(mon_noeud);
        genereComplementaires(mon_etat, mon_noeud->vue, joueur);
        mon_coup->score += simule(mon_etat, mon_coup, phase, joueur);
        mon_coup->n_exec++;
        mon_noeud->n_parties = 1;
        return simule(mon_etat, mon_coup, phase, joueur);
    }
    else
    {
        free(ma_vue);
        mon_coup = selectionne(rech->noeud);
        genereComplementaires(mon_etat, rech->noeud->vue, joueur);
        if (phase == 1)
        {
            mon_coup->coup_p1->joueur = joueur;
            appliqueP1(mon_etat, mon_coup->coup_p1);
            float s = developpe(mon_etat, joueur, 2); // simule(mon_etat, mon_coup, phase, joueur);
            mon_coup->score += s;
            mon_coup->n_exec++;
            rech->noeud->n_parties++;
            return s;
        }
        else
        {
            mon_coup->coup_p2->joueur = joueur;
            appliqueP2(mon_etat, mon_coup->coup_p2);
            float s = 1 - developpe(mon_etat, !joueur, 1); // simule(mon_etat, mon_coup, phase, joueur);
            mon_coup->score += s;
            mon_coup->n_exec++;
            rech->noeud->n_parties++;
            return s;
        }
    }
}

int *genereCarteNonRetourneeAlea(etat *mon_etat, int joueur)
{
    int coord_carte[12][2];
    int compteur = 0;
    /* Pour chaque carte du joueur */
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            /* Si la carte n'est pas retournée */
            if (!mon_etat->mains[joueur][i][j].est_retournee)
            {
                coord_carte[compteur][0] = i;
                coord_carte[compteur][1] = j;
                compteur++;
            }
        }
    }

    /* On genere un entier aléatoire suivant le nombre de carte face cachée */
    int k = rand() % compteur;
    int *coord = (int *)malloc(2 * sizeof(int));
    coord[0] = coord_carte[k][0];
    coord[1] = coord_carte[k][1];

    return coord;
}

liste_coups *selectMeilleurCoup(liste_coups *coups_possibles)
{
    liste_coups *cour = coups_possibles;
    liste_coups *meilleur_coup = NULL;
    float meilleur_score = -1;
    if (cour != NULL)
    {
        meilleur_coup = cour;
        meilleur_score = cour->score / cour->n_exec;
        cour = cour->suiv;
    }

    while (cour != NULL)
    {
        if (cour->score / cour->n_exec > meilleur_score)
        {
            meilleur_coup = cour;
            meilleur_score = cour->score / cour->n_exec;
        }
        cour = cour->suiv;
    }

    return meilleur_coup;
}

coup_p2 *choisirCoup(etat *mon_etat, int joueur)
{
    /* On copie l'etat pour pouvoir le manipuler */
    etat *mon_etat_copie = NULL;

    /* On créé le noeud racine */
    noeud *racine1 = NULL;
    vue *ma_vue = genereVue(mon_etat, joueur, 1);
    noeud_rn *rech = rechercheNoeudRN(arbre, ma_vue);
    liste_coups *mon_coup = NULL;
    if (rech == NULL)
    {
        racine1 = (noeud *)malloc(sizeof(noeud));
        racine1->vue = ma_vue;
        racine1->n_parties = 0;
        genereCoups(racine1);
        arbre = insereNoeudRN(arbre, racine1);
    }
    else
    {
        free(ma_vue);
        racine1 = rech->noeud;
    }

    /* Tant qu'on est dans le temps imparti */
    clock_t debut = clock();
    while (((clock() - debut) * 1000 / CLOCKS_PER_SEC) < 500)
    {
        prof = 0;
        mon_etat_copie = copieJeu(mon_etat);
        /* On selectionne le coup à améliorer */
        mon_coup = selectionne(racine1);
        /* On genere un complémentaire associé au coup */
        genereComplementaires(mon_etat_copie, racine1->vue, joueur);
        /* On applique le coup de la phase I en fonction du complémentaire */
        mon_coup->coup_p1->joueur = joueur;
        appliqueP1(mon_etat_copie, mon_coup->coup_p1); // mon_etat_copie est modifié
        mon_coup->score += developpe(mon_etat_copie, joueur, 2);
        mon_coup->n_exec++;
        racine1->n_parties++;
        free(mon_etat_copie);
    }

    // liste_coups *cour = racine1->coups;
    // while (cour != NULL)
    // {
    //     printf("%d %d %f\n", cour->coup_p1->type, cour->n_exec, cour->score);
    //     cour = cour->suiv;
    // }

    mon_coup = selectMeilleurCoup(racine1->coups);
    mon_coup->coup_p1->joueur = joueur;
    appliqueP1(mon_etat, mon_coup->coup_p1);
    // printf("%d\n", mon_coup->coup_p1->type);

    noeud *racine2 = NULL;
    ma_vue = genereVue(mon_etat, joueur, 2);
    rech = rechercheNoeudRN(arbre, ma_vue);
    if (rech == NULL)
    {
        racine2 = (noeud *)malloc(sizeof(noeud));
        racine2->vue = ma_vue;
        racine2->n_parties = 0;
        genereCoups(racine2);
        arbre = insereNoeudRN(arbre, racine2);
    }
    else
    {
        free(ma_vue);
        racine2 = rech->noeud;
    }

    debut = clock();
    while (((clock() - debut) * 1000 / CLOCKS_PER_SEC) < 6000)
    {
        prof = 0;
        mon_etat_copie = copieJeu(mon_etat);
        /* On selectionne le coup à améliorer */
        mon_coup = selectionne(racine2);
        /* On genere un complémentaire associé au coup */
        genereComplementaires(mon_etat_copie, racine2->vue, joueur);
        /* On applique le coup de la phase I en fonction du complémentaire */
        mon_coup->coup_p2->joueur = joueur;
        appliqueP2(mon_etat_copie, mon_coup->coup_p2); // mon_etat_copie est modifié
        float s = 1 - developpe(mon_etat_copie, !joueur, 1);
        mon_coup->score += s;
        mon_coup->n_exec++;
        racine2->n_parties++;
        // afficheJeuTexte(mon_etat_copie);
        // printf("%d\n", calculeScore(mon_etat_copie, joueur));
        // printf("%d\n", calculeScore(mon_etat_copie, !joueur));
        // printf("%f\n", s);
        // printf("%d %d %d %d\n", mon_coup->coup_p2->type, mon_coup->coup_p2->carte_entree, mon_coup->coup_p2->coord_carte[0], mon_coup->coup_p2->coord_carte[1]);
        free(mon_etat_copie);
    }

    // liste_coups *cour = racine2->coups;
    // while (cour != NULL)
    // {
    //     printf("%d %d %d %d %d %f %f\n", cour->coup_p2->type, cour->coup_p2->carte_entree, cour->coup_p2->coord_carte[0], cour->coup_p2->coord_carte[1], cour->n_exec, cour->score / cour->n_exec, UCB(cour, racine2->n_parties));
    //     cour = cour->suiv;
    // }

    mon_coup = selectMeilleurCoup(racine2->coups);
    mon_coup->coup_p2->joueur = joueur;

    return mon_coup->coup_p2;
}

noeud_rn *creeNoeudRN(noeud *noeud)
{
    noeud_rn *racine = (noeud_rn *)malloc(sizeof(noeud_rn));
    if (racine == NULL)
    {
        return NULL;
    }
    racine->couleur = ROUGE;
    racine->droite = NULL;
    racine->gauche = NULL;
    racine->parent = NULL;
    racine->noeud = noeud;
    return racine;
}

noeud_rn *rotationDroite(noeud_rn *y)
{
    if (y != NULL)
    {
        if (y->gauche != NULL)
        {
            noeud_rn *x = y->gauche;
            y->gauche = x->droite;
            if (x->droite != NULL)
            {
                x->droite->parent = y;
            }
            x->droite = y;
            y->parent = x;
            return x;
        }
    }
    return y;
}

noeud_rn *rotationGauche(noeud_rn *x)
{
    if (x != NULL)
    {
        if (x->droite != NULL)
        {
            noeud_rn *y = x->droite;
            x->droite = y->gauche;
            if (y->gauche != NULL)
            {
                y->gauche->parent = x;
            }
            y->gauche = x;
            x->parent = y;
            return y;
        }
    }
    return x;
}

noeud_rn *insereNoeudRN(noeud_rn *racine, noeud *nouv)
{
    if (racine == NULL)
    {
        racine = creeNoeudRN(nouv);
    }
    else
    {
        noeud_rn *cour = racine;

        int cmp = memcmp(nouv->vue, cour->noeud->vue, sizeof(vue));
        while ((cmp < 0 && cour->gauche != NULL) || (cmp > 0 && cour->droite != NULL))
        {
            if (cmp < 0)
            {
                cour = cour->gauche;
            }
            else if (cmp > 0)
            {
                cour = cour->droite;
            }
            cmp = memcmp(nouv->vue, cour->noeud->vue, sizeof(vue));
        }

        if (cmp != 0)
        {
            if (cmp < 0)
            {
                cour->gauche = creeNoeudRN(nouv);
                cour->gauche->parent = cour;
                cour = cour->gauche;
            }
            else if (cmp > 0)
            {
                cour->droite = creeNoeudRN(nouv);
                cour->droite->parent = cour;
                cour = cour->droite;
            }

            while (cour != NULL)
            {
                if (cour->parent != NULL && cour->parent->couleur == ROUGE)
                {
                    if (cour->parent->parent == NULL)
                    {
                        cour->parent->couleur = NOIR;
                        cour = NULL;
                    }
                    else
                    {
                        if (cour->parent->parent->gauche != NULL && cour->parent->parent->droite != NULL && cour->parent == cour->parent->parent->gauche && cour->parent->parent->droite->couleur == ROUGE)
                        {
                            cour->parent->couleur = NOIR;
                            cour->parent->parent->droite->couleur = NOIR;
                            cour->parent->parent->couleur = ROUGE;
                            cour = cour->parent->parent;
                        }
                        else if (cour->parent->parent->gauche != NULL && cour->parent->parent->droite != NULL && cour->parent == cour->parent->parent->droite && cour->parent->parent->gauche->couleur == ROUGE)
                        {
                            cour->parent->couleur = NOIR;
                            cour->parent->parent->gauche->couleur = NOIR;
                            cour->parent->parent->couleur = ROUGE;
                            cour = cour->parent->parent;
                        }
                        else
                        {
                            if (cour->parent->parent->droite != NULL && cour->parent->gauche != NULL && cour->parent == cour->parent->parent->droite && cour == cour->parent->gauche)
                            {
                                cour = cour->parent->parent;
                                cour->droite = rotationDroite(cour->droite);
                                cour->droite->parent = cour;
                                cour = cour->droite->droite;
                            }
                            else if (cour->parent->parent->gauche != NULL && cour->parent->droite != NULL && cour->parent == cour->parent->parent->gauche && cour == cour->parent->droite)
                            {
                                cour = cour->parent->parent;
                                cour->gauche = rotationGauche(cour->gauche);
                                cour->gauche->parent = cour;
                                cour = cour->gauche->gauche;
                            }
                            else
                            {
                                if (cour->parent->parent->gauche != NULL && cour->parent == cour->parent->parent->gauche)
                                {
                                    if (cour->parent->parent->parent == NULL)
                                    {
                                        cour = rotationDroite(cour->parent->parent);
                                        cour->parent = NULL;
                                        cour->couleur = !cour->couleur;
                                        cour->droite->couleur = !cour->droite->couleur;
                                        racine = cour;
                                    }
                                    else
                                    {
                                        if (cour->parent->parent->parent->gauche != NULL && cour->parent->parent == cour->parent->parent->parent->gauche)
                                        {
                                            cour = cour->parent->parent->parent;
                                            cour->gauche = rotationDroite(cour->gauche);
                                            cour->gauche->parent = cour;
                                            cour->gauche->couleur = !cour->gauche->couleur;
                                            cour->gauche->droite->couleur = !cour->gauche->droite->couleur;
                                        }
                                        else if (cour->parent->parent->parent->droite != NULL && cour->parent->parent == cour->parent->parent->parent->droite)
                                        {
                                            cour = cour->parent->parent->parent;
                                            cour->droite = rotationDroite(cour->droite);
                                            cour->droite->parent = cour;
                                            cour->droite->couleur = !cour->droite->couleur;
                                            cour->droite->droite->couleur = !cour->droite->droite->couleur;
                                        }
                                    }
                                }
                                else if (cour->parent->parent->droite != NULL && cour->parent == cour->parent->parent->droite)
                                {
                                    if (cour->parent->parent->parent == NULL)
                                    {
                                        cour = rotationGauche(cour->parent->parent);
                                        cour->parent = NULL;
                                        cour->couleur = !cour->couleur;
                                        cour->gauche->couleur = !cour->gauche->couleur;
                                        racine = cour;
                                    }
                                    else
                                    {
                                        if (cour->parent->parent->parent->gauche != NULL && cour->parent->parent == cour->parent->parent->parent->gauche)
                                        {
                                            cour = cour->parent->parent->parent;
                                            cour->gauche = rotationGauche(cour->gauche);
                                            cour->gauche->parent = cour;
                                            cour->gauche->couleur = !cour->gauche->couleur;
                                            cour->gauche->gauche->couleur = !cour->gauche->gauche->couleur;
                                        }
                                        else if (cour->parent->parent->parent->droite != NULL && cour->parent->parent == cour->parent->parent->parent->droite)
                                        {
                                            cour = cour->parent->parent->parent;
                                            cour->droite = rotationGauche(cour->droite);
                                            cour->droite->parent = cour;
                                            cour->droite->couleur = !cour->droite->couleur;
                                            cour->droite->gauche->couleur = !cour->droite->gauche->couleur;
                                        }
                                    }
                                }
                                cour = NULL;
                            }
                        }
                    }
                }
                else
                {
                    cour = NULL;
                }
            }
        }
    }

    return racine;
}

noeud_rn *rechercheNoeudRN(noeud_rn *racine, vue *ma_vue)
{
    if (racine == NULL)
    {
        return NULL;
    }
    noeud_rn *cour = racine;
    int cmp = memcmp(ma_vue, cour->noeud->vue, sizeof(vue));

    while (cour != NULL && cmp)
    {
        if (cmp < 0)
        {
            cour = cour->gauche;
        }
        else
        {
            cour = cour->droite;
        }
        if (cour != NULL)
        {
            cmp = memcmp(ma_vue, cour->noeud->vue, sizeof(vue));
        }
    }

    return cour;
}