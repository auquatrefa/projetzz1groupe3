#include "jeu.h"

/****************************************************/
/* initTabPioche()                                  */
/*                                                  */
/* Initialisation du tableau de la pioche           */
/*                                                  */
/* Entrées :                                        */
/*                                                  */
/* Sortie :                                         */
/*  - int * : tableau de la pioche                  */
/*                                                  */
/****************************************************/
int *initTabPioche()
{
    int *tab_pioche = (int *)malloc(150 * sizeof(int));
    int i;

    for (i = 0; i < 5; i++) // remplissage des -2
        tab_pioche[i] = -2;
    for (i = 5; i < 20; i++) // remplissage des 0
        tab_pioche[i] = 0;
    for (i = 20; i < 30; i++) // remplissage des -1
        tab_pioche[i] = -1;
    for (i = 30; i < 40; i++) // remplissage des 1
        tab_pioche[i] = 1;
    for (i = 40; i < 50; i++) // remplissage des 2
        tab_pioche[i] = 2;
    for (i = 50; i < 60; i++) // remplissage des 3
        tab_pioche[i] = 3;
    for (i = 60; i < 70; i++) // remplissage des 4
        tab_pioche[i] = 4;
    for (i = 70; i < 80; i++) // remplissage des 5
        tab_pioche[i] = 5;
    for (i = 80; i < 90; i++) // remplissage des 6
        tab_pioche[i] = 6;
    for (i = 90; i < 100; i++) // remplissage des 7
        tab_pioche[i] = 7;
    for (i = 100; i < 110; i++) // remplissage des 8
        tab_pioche[i] = 8;
    for (i = 110; i < 120; i++) // remplissage des 9
        tab_pioche[i] = 9;
    for (i = 120; i < 130; i++) // remplissage des 10
        tab_pioche[i] = 10;
    for (i = 130; i < 140; i++) // remplissage des 11
        tab_pioche[i] = 11;
    for (i = 140; i < 150; i++) // remplissage des 12
        tab_pioche[i] = 12;

    return melangeTabPioche(tab_pioche);
}

/****************************************************/
/* melangeTabPioche()                               */
/*                                                  */
/* Mélange du tableau de la pioche par Fisher-Yates */
/*                                                  */
/* Entrées :                                        */
/*  - int * : tableau de la pioche                  */
/*                                                  */
/* Sortie :                                         */
/*  - int * : tableau de la pioche mélangé          */
/*                                                  */
/****************************************************/
int *melangeTabPioche(int *tab)
{
    int temp, i, j;
    for (i = 149; i >= 1; i--)
    {
        j = rand() % (i + 1);
        temp = tab[i];
        tab[i] = tab[j];
        tab[j] = temp;
    }

    return tab;
}

/****************************************************/
/* piocheCarte()                                    */
/*                                                  */
/* Pioche une carte dans la pioche                  */
/*                                                  */
/* Entrées :                                        */
/*  - etat * : état du jeu                          */
/*                                                  */
/* Sortie :                                         */
/*  - int : valeur de la carte piochée              */
/*                                                  */
/****************************************************/
int piocheCarte(etat *mon_etat)
{
    int ma_carte = mon_etat->pioche->carte_deb->valeur;
    carte_pioche *temp = mon_etat->pioche->carte_deb;
    mon_etat->pioche->carte_deb = mon_etat->pioche->carte_deb->suiv;
    free(temp);

    return ma_carte;
}

/****************************************************/
/* tetePioche()                                     */
/*                                                  */
/*  Retourne la valeur de la carte en tête de pioche*/
/*                                                  */
/* Entrées :                                        */
/*  - etat * : état du jeu                          */
/*                                                  */
/* Sortie :                                         */
/*  - int : valeur de la carte en tête de pioche    */
/*                                                  */
/****************************************************/
int tetePioche(etat *mon_etat)
{
    return mon_etat->pioche->carte_deb->valeur;
}

/****************************************************/
/* ajoutePioche()                                   */
/*                                                  */
/* Ajoute une carte à la fin de la pioche           */
/*                                                  */
/* Entrées :                                        */
/*  - etat * : état du jeu                          */
/*  - int : valeur de la carte à ajouter            */
/*                                                  */
/* Sortie :                                         */
/*                                                  */
/****************************************************/
void ajoutePioche(etat *mon_etat, int carte)
{
    // on créé la carte de la pioche
    carte_pioche *ma_carte = (carte_pioche *)malloc(sizeof(carte_pioche));
    ma_carte->valeur = carte;
    ma_carte->suiv = NULL;

    // on l'ajoute à la fin
    if (mon_etat->pioche->carte_deb == NULL)
    {
        mon_etat->pioche->carte_fin = ma_carte;
        mon_etat->pioche->carte_deb = ma_carte;
    }
    else
    {
        mon_etat->pioche->carte_fin->suiv = ma_carte;
        mon_etat->pioche->carte_fin = ma_carte;
    }
}

/****************************************************/
/* liberePioche()                                   */
/*                                                  */
/* Libère la mémoire de la pioche                   */
/*                                                  */
/* Entrées :                                        */
/*  - pioche * : pioche à libérer                   */
/*                                                  */
/* Sortie :                                         */
/*                                                  */
/****************************************************/
void liberePioche(pioche *ma_pioche)
{
    carte_pioche *cour = ma_pioche->carte_deb;
    while (cour != NULL)
    {
        carte_pioche *temp = cour;
        cour = cour->suiv;
        free(temp);
    }
    free(ma_pioche);
}

/****************************************************/
/* initJeu()                                        */
/*                                                  */
/* Initialisation du jeu                            */
/*                                                  */
/* Entrées :                                        */
/*                                                  */
/* Sortie :                                         */
/*  - etat * : état du jeu                          */
/*                                                  */
/****************************************************/
etat *initJeu()
{
    etat *mon_etat = (etat *)malloc(sizeof(etat));

    /* Initialisation de la pioche */
    mon_etat->pioche = (pioche *)malloc(sizeof(pioche));
    mon_etat->pioche->carte_deb = NULL;
    mon_etat->pioche->carte_fin = NULL;
    int *tab_pioche = initTabPioche();
    for (int i = 0; i < 150; i++)
    {
        ajoutePioche(mon_etat, tab_pioche[i]);
    }

    /* Initialisation de la defausse */
    mon_etat->defausse = piocheCarte(mon_etat);
    mon_etat->carte_piochee = -3;

    /* Initialisation des mains */
    for (int joueur = 0; joueur < 2; joueur++)
    {
        for (int ligne = 0; ligne < 3; ligne++)
        {
            for (int colonne = 0; colonne < 4; colonne++)
            {
                carte ma_carte;
                ma_carte.valeur = piocheCarte(mon_etat);
                ma_carte.est_retournee = 0;
                mon_etat->mains[joueur][ligne][colonne] = ma_carte;
            }
        }
    }

    free(tab_pioche);

    return mon_etat;
}

void actualiseDefausse(etat *mon_etat, int carte)
{
    mon_etat->defausse = carte;
}

void retourneCarte(etat *mon_etat, int joueur, int *coord_carte)
{
    mon_etat->mains[joueur][coord_carte[0]][coord_carte[1]].est_retournee = 1;
}

void echangeCarte(etat *mon_etat, int joueur, int *coord_carte, int carte)
{
    actualiseDefausse(mon_etat, mon_etat->mains[joueur][coord_carte[0]][coord_carte[1]].valeur);
    mon_etat->mains[joueur][coord_carte[0]][coord_carte[1]].valeur = carte;
    mon_etat->mains[joueur][coord_carte[0]][coord_carte[1]].est_retournee = 1;
}

void verifColonne(etat *mon_etat, int joueur)
{
    for (int j = 0; j < 4; ++j)
    {
        if (mon_etat->mains[joueur][0][j].valeur == mon_etat->mains[joueur][1][j].valeur && mon_etat->mains[joueur][1][j].valeur == mon_etat->mains[joueur][2][j].valeur && mon_etat->mains[joueur][0][j].est_retournee && mon_etat->mains[joueur][1][j].est_retournee && mon_etat->mains[joueur][2][j].est_retournee)
        {
            for (int i = 0; i < 3; ++i)
            {
                mon_etat->mains[joueur][i][j].valeur = -3;
            }
        }
    }
}

void appliqueP1(etat *mon_etat, coup_p1 *mon_coup)
{
    switch (mon_coup->type)
    {
    case 1:
        ajoutePioche(mon_etat, mon_etat->defausse);
        mon_etat->carte_piochee = piocheCarte(mon_etat);
        break;
    default:
        break;
    }
}

void appliqueP2(etat *mon_etat, coup_p2 *mon_coup)
{
    switch (mon_coup->type)
    {
    case 0:
        echangeCarte(mon_etat, mon_coup->joueur, mon_coup->coord_carte, mon_coup->carte_entree);
        break;
    case 1:
        actualiseDefausse(mon_etat, mon_coup->carte_entree);
        retourneCarte(mon_etat, mon_coup->joueur, mon_coup->coord_carte);
        break;
    default:
        break;
    }
    verifColonne(mon_etat, mon_coup->joueur);
    mon_etat->carte_piochee = -3;
}

etat *copieJeu(etat *mon_etat)
{
    etat *mon_etat_bis = (etat *)malloc(sizeof(etat));
    mon_etat_bis->pioche = (pioche *)malloc(sizeof(pioche));
    mon_etat_bis->pioche->carte_deb = NULL;
    mon_etat_bis->pioche->carte_fin = NULL;
    carte_pioche *cour = mon_etat->pioche->carte_deb;
    while (cour != NULL)
    {
        ajoutePioche(mon_etat_bis, cour->valeur);
        cour = cour->suiv;
    }
    mon_etat_bis->defausse = mon_etat->defausse;
    mon_etat_bis->carte_piochee = mon_etat->carte_piochee;

    for (int joueur = 0; joueur < 2; joueur++)
    {
        for (int ligne = 0; ligne < 3; ligne++)
        {
            for (int colonne = 0; colonne < 4; colonne++)
            {
                carte ma_carte;
                ma_carte.valeur = mon_etat->mains[joueur][ligne][colonne].valeur;
                ma_carte.est_retournee = mon_etat->mains[joueur][ligne][colonne].est_retournee;
                mon_etat_bis->mains[joueur][ligne][colonne] = ma_carte;
            }
        }
    }

    return mon_etat_bis;
}

int aFini(etat *mon_etat, int joueur)
{
    int i = 0;
    while (i < 12 && mon_etat->mains[joueur][i % 3][i / 3].est_retournee)
    {
        ++i;
    }
    return i == 12;
}

void decouvreJeu(etat *mon_etat, int joueur)
{
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            mon_etat->mains[joueur][i][j].est_retournee = 1;
        }
    }
}

int calculeScore(etat *mon_etat, int joueur)
{
    int somme = 0;
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            somme += mon_etat->mains[joueur][i][j].valeur;
        }
    }
    return somme;
}

void afficheJeuTexte(etat *mon_etat)
{
    for (int k = 0; k < 2; ++k)
    {
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                if (mon_etat->mains[k][i][j].est_retournee)
                {
                    if (mon_etat->mains[k][i][j].valeur > -1 && mon_etat->mains[k][i][j].valeur < 10)
                    {
                        printf(" %d | ", mon_etat->mains[k][i][j].valeur);
                    }
                    else if (mon_etat->mains[k][i][j].valeur < -2)
                    {
                        printf(" x | ");
                    }
                    else
                    {
                        printf("%d | ", mon_etat->mains[k][i][j].valeur);
                    }
                }
                else
                {
                    printf("   | ");
                }
            }
            printf("\n");
        }
        printf("\n");
    }
}

void libereJeu(etat *mon_etat)
{
    liberePioche(mon_etat->pioche);
    free(mon_etat);
}