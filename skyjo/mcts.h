#ifndef mcts_h
#define mcts_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "jeu.h"

enum couleur_noeud
{
    ROUGE,
    NOIR
};

typedef struct vue
{
    int ma_main[3][4];
    int defausse;
    int carte_piochee;
    int phase; // 1: phase 1 | 2: phase 2
} vue;

typedef struct liste_coups
{
    union
    {
        coup_p1 *coup_p1;
        coup_p2 *coup_p2;
    };
    int n_exec;
    float score;
    struct liste_coups *suiv;
} liste_coups;

typedef struct noeud
{
    vue *vue;
    int n_parties;
    liste_coups *coups;
} noeud;

typedef struct noeud_rn
{
    noeud *noeud;
    struct noeud_rn *gauche;
    struct noeud_rn *droite;
    struct noeud_rn *parent;
    int couleur;
} noeud_rn;

noeud_rn *creeNoeudRN(noeud *noeud);
noeud_rn *rotationDroite(noeud_rn *y);
noeud_rn *rotationGauche(noeud_rn *x);
noeud_rn *insereNoeudRN(noeud_rn *racine, noeud *nouv);
noeud_rn *rechercheNoeudRN(noeud_rn* racine, vue *ma_vue);
int piocheProba();
vue *genereVue(etat *mon_etat, int joueur, int phase);
void genereCoups(noeud *mon_noeud);
float UCB(liste_coups *coup, int N);
liste_coups *selectionne(noeud* mon_noeud);
void genereComplementaires(etat *mon_etat, vue *ma_vue, int joueur);
float simule(etat *mon_etat, liste_coups *coup, int phase, int joueur);
float developpe(etat *mon_etat, int joueur, int phase);
int *genereCarteNonRetourneeAlea(etat *mon_etat, int joueur);
coup_p2 *choisirCoup(etat *mon_etat, int joueur);
liste_coups *selectMeilleurCoup(liste_coups * coups_possibles);

#endif