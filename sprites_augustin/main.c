#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

/* VARIABLES GLOBALES */
SDL_Window   * fenetre   = NULL; // fenetre utilisee par le programme
int            largeur    = 870;  // largeur de la fenetre
int            hauteur   = 480;  // hauteur de la fenetre
SDL_Renderer   *rendu = NULL;   // moteur de rendu SDL

/* FONCTIONS */

  void play_with_texture_1(SDL_Texture *my_texture, SDL_Window *window,
               SDL_Renderer *renderer) {
    SDL_Rect 
        source = {0},                         // Rectangle définissant la zone de la texture à récupérer
        window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
        destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

    SDL_GetWindowSize(
    window, &window_dimensions.w,
    &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
    SDL_QueryTexture(my_texture, NULL, NULL,
             &source.w, &source.h);       // Récupération des dimensions de l'image

    destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

    /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

    SDL_RenderCopy(renderer, my_texture,
           &source,
           &destination);                 // Création de l'élément à afficher
    SDL_RenderPresent(renderer);                  // Affichage
    //SDL_Delay(2000);                              // Pause en ms

    //SDL_RenderClear(renderer);                    // Effacer la fenêtre
  }

void play_with_texture_4(SDL_Texture* my_texture, SDL_Window* window, SDL_Renderer* renderer, SDL_Texture *my_texture2) {
     SDL_Rect 
           source = {0},                    // Rectangle définissant la zone totale de la planche
           window_dimensions = {0},         // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
           destination = {0},               // Rectangle définissant où la zone_source doit être déposée dans le renderer
           state = {0};                     // Rectangle de la vignette en cours dans la planche 

     SDL_GetWindowSize(window,              // Récupération des dimensions de la fenêtre
               &window_dimensions.w,
               &window_dimensions.h);
     SDL_QueryTexture(my_texture,           // Récupération des dimensions de l'image
              NULL, NULL,
              &source.w, &source.h);

     /* Mais pourquoi prendre la totalité de l'image, on peut n'en afficher qu'un morceau, et changer de morceau :-) */

     int nb_images = 6;                     // Il y a 6 vignette dans la ligne de l'image qui nous intéresse
     float zoom = 1;                        // zoom, car ces images sont un peu petites
     int offset_x = source.w / nb_images,   // La largeur d'une vignette de l'image, marche car la planche est bien réglée
         offset_y = source.h / 8;           // La hauteur d'une vignette de l'image, marche car la planche est bien réglée
       

     state.x = 0 ;                          // La première vignette est en début de ligne
     state.y = 2 * offset_y;                // On s'intéresse à la 3ème ligne, le bonhomme qui court
     state.w = offset_x;                    // Largeur de la vignette
     state.h = offset_y;                    // Hauteur de la vignette

     destination.w = offset_x * zoom;       // Largeur du sprite à l'écran
     destination.h = offset_y * zoom;       // Hauteur du sprite à l'écran

     destination.y =                        // La course se fait en bas de l'écran (en vertical)
       window_dimensions.h - ((window_dimensions.h - destination.h) /2);

     int speed = 3;
     destination.x = window_dimensions.w / 3;
     for (int x = 0; x < 100; x += speed) {
       //destination.x = x;                   // Position en x pour l'affichage du sprite
       state.x += offset_x;                 // On passe à la vignette suivante dans l'image
       state.x %= source.w;                 // La vignette qui suit celle de fin de ligne est celle de début de ligne

       SDL_RenderClear(renderer);           // Effacer l'image précédente avant de dessiner la nouvelle
       play_with_texture_1(my_texture2, window, renderer);
       SDL_RenderCopy(renderer, my_texture, // Préparation de l'affichage
              &state,
              &destination);  
       SDL_RenderPresent(renderer);         // Affichage
       SDL_Delay(100);                       // Pause en ms
     }
     //SDL_RenderClear(renderer);             // Effacer la fenêtre avant de rendre la main
       }

int main(int argc, char **argv){

    (void) argc;
    (void) argv;
    int tourne = 1;
    int cpt = 0;
    SDL_Event evenement;

    fenetre = SDL_CreateWindow("Fenêtre Principale", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                largeur, hauteur, 
                SDL_WINDOW_RESIZABLE); 
        
    if (fenetre == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    rendu = SDL_CreateRenderer(fenetre, -1, SDL_RENDERER_ACCELERATED );
    if (rendu == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
    }

    SDL_Texture *ma_texture; 
    ma_texture = IMG_LoadTexture(rendu,"./images/Warrior_Red.png");
    //if (ma_texture == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", fenetre, rendu);

    SDL_Texture *ma_texture2; 
    ma_texture2 = IMG_LoadTexture(rendu,"./images/fondEcran.jpg");
    //if (ma_texture2 == NULL) end_sdl(0, "Echec du chargement de l'image dans la texture", fenetre, rendu);

    while(tourne){
        while(SDL_PollEvent(&evenement)){
            switch(evenement.type){
                case SDL_WINDOWEVENT:
                    switch (evenement.window.event){
                        case SDL_WINDOWEVENT_CLOSE:
                            printf("Appui sur la croix\n");
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            largeur = evenement.window.data1;
                            hauteur = evenement.window.data2;
                            break;
                    }
                    break;
                case SDL_QUIT:
                    printf("On quitte\n");
                    tourne = 0;
                    break;
                default:
                    break;
            }
            
            //play_with_texture_1(ma_texture2, fenetre, rendu);
            if(cpt > 10){
                tourne = 0;
            }else{
                cpt++;
            }
            printf("%d\n",cpt);
            play_with_texture_4(ma_texture, fenetre, rendu, ma_texture2);
            SDL_RenderClear(rendu);
        }
        SDL_Delay(1);
    }

    IMG_Quit(); 

    SDL_DestroyTexture(ma_texture);
    SDL_DestroyRenderer(rendu);
    SDL_DestroyWindow(fenetre);
    SDL_Quit();
    return 0;
}