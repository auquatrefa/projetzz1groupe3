#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    SDL_Window *window = NULL; // initialisation fenetre

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n",
                SDL_GetError()); // l'initialisation de la SDL a échoué
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode mode;
    SDL_GetCurrentDisplayMode(0, &mode);

    window = SDL_CreateWindow("X Fenêtre", (mode.w - 400) / 2, (mode.h - 300) / 2, 400, 300,
                              SDL_WINDOW_RESIZABLE);

    if (window == NULL)
    {
        SDL_Log("Error : SDL window creation - %s\n",
                SDL_GetError()); // la création de la fenêtre a échoué
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    int x = (mode.w - 400) / 2, y = (mode.h - 300) / 2;
    int h = 300, w = 400;

    SDL_bool
        program_on = SDL_TRUE,   // programme doit continuer
        event_utile = SDL_FALSE; // evenement en cours
    SDL_Event event;

    while (program_on)
    {
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        { // si on a un evenement dans la file
            switch (event.type)
            {
            case SDL_QUIT:
                program_on = SDL_FALSE;
                event_utile = SDL_TRUE;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_RIGHT:
                    SDL_GetWindowPosition(window, &x, &y);
                    SDL_GetWindowSize(window, &w, &h);
                    SDL_SetWindowPosition(window, x + 10, y);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_LEFT:
                    SDL_GetWindowPosition(window, &x, &y);
                    SDL_GetWindowSize(window, &w, &h);
                    SDL_SetWindowPosition(window, x - 10, y);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_UP:
                    SDL_GetWindowPosition(window, &x, &y);
                    SDL_GetWindowSize(window, &w, &h);
                    SDL_SetWindowPosition(window, x, y - 10);
                    event_utile = SDL_TRUE;
                    break;
                case SDLK_DOWN:
                    SDL_GetWindowPosition(window, &x, &y);
                    SDL_GetWindowSize(window, &w, &h);
                    SDL_SetWindowPosition(window, x, y + 10);
                    event_utile = SDL_TRUE;
                    break;
                }
            }
        }
    }

    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}