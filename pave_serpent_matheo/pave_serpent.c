#include <stdio.h>
#include <stdlib.h>

#include "pave_serpent.h"

#include <SDL2/SDL.h>

#define RECT_W 50
#define RECT_H 50

/*********************************************************************************/
/* Fonction dessinerRectangle                                                    */
/*                                                                               */
/* Fonction permettant de dessiner un rectangle de couleur donnée                */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - SDL_Renderer* renderer : pointeur sur le renderer                      */
/*      - int x : position en abscisse du rectangle                              */
/*      - int y : position en ordonnée du rectangle                              */
/*      - int r : couleur rouge du rectangle                                     */
/*      - int g : couleur verte du rectangle                                     */
/*      - int b : couleur bleue du rectangle                                     */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void dessinerRectangle(SDL_Renderer* renderer, int x, int y, int r, int g, int b)
{
    SDL_Rect rectangle;
    rectangle.x = x;
    rectangle.y = y;
    rectangle.w = RECT_W;
    rectangle.h = RECT_H;

    SDL_SetRenderDrawColor(renderer, r, g, b, 255); // on lui donne une couleur
    SDL_RenderFillRect(renderer, &rectangle);
}

/*********************************************************************************/
/* Fonction afficheFenetre                                                       */
/*                                                                               */
/* Fonction permettant d'afficher une fenêtre                                    */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void afficheFenetre()
{
    donnees_f_t ma_fenetre;
    int graphics = 1;

    SDL_DisplayMode mode;

    // CREATION SDL

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
    { // initialisation SDL
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        graphics = 0;
    }
    else
    {
        SDL_GetCurrentDisplayMode(0, &mode); // données de l'ecran

        ma_fenetre.width = mode.w / 2;  // moitie de l'ecran en largeur
        ma_fenetre.height = mode.h / 2; // moitie de l'ecran en hauteur

        // CREATION FENETRE

        ma_fenetre.window = SDL_CreateWindow("Pavé Serpent", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                             ma_fenetre.width, ma_fenetre.height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        if (ma_fenetre.window == 0)
        {
            fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
            graphics = 0;
        }
        else
        {
            // CREATION RENDERER

            ma_fenetre.renderer = SDL_CreateRenderer(ma_fenetre.window, -1, SDL_RENDERER_ACCELERATED);
            if (ma_fenetre.renderer == 0)
            {
                fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
                graphics = 0;
            }
        }
    }

    if (graphics == 0)
    {
        if (ma_fenetre.renderer != 0)
            SDL_DestroyRenderer(ma_fenetre.renderer);
        if (ma_fenetre.window != 0)
            SDL_DestroyWindow(ma_fenetre.window);

        SDL_Quit();
        return;
    }

    // ECOUTE ET GESTION DES EVENEMENTS

    int xPosRect = 10; // position en abscisse du rectangle
    int yPosRect = 20; // position en ordonnée du rectangle
    int directionRectW = 10; // direction du rectangle en abscisse
    int directionRectH = 10; // direction du rectangle en ordonnée
    int couleurRectR = 255; // couleur rouge du rectangle
    int couleurRectG = 0; // couleur verte du rectangle
    int couleurRectB = 0; // couleur bleue du rectangle

    SDL_SetRenderDrawColor(ma_fenetre.renderer, 0, 0, 0, 255); // on met le fond en noir

    SDL_bool
        program_on = SDL_TRUE,
        event_utile = SDL_FALSE;
    SDL_Event event;

    int deplacementRect = 0; // 0 : pas de déplacement, 1 : déplacement
    int changeCouleurRect = 0; // 0 : pas de changement, 1 : changement

    while (program_on)
    {
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT: // on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE;
                event_utile = SDL_TRUE;
                break;

            case SDL_KEYDOWN: // Le type de event est : une touche appuyée

                switch (event.key.keysym.sym)
                {
                case SDLK_SPACE: // 'SPACE'
                    deplacementRect = !deplacementRect;
                    break;

                case SDLK_RETURN: // 'RETURN'
                    changeCouleurRect = !changeCouleurRect;
                    break;

                default: // Une touche appuyée qu'on ne traite pas
                    break;
                }
                break;

            case SDL_MOUSEBUTTONDOWN: // Click souris
                // on recupere la position de la souris
                xPosRect = event.button.x;
                yPosRect = event.button.y;
                break;

            default: // Les évènements qu'on n'a pas envisagé
                break;
            }
        }

        SDL_SetRenderDrawColor(ma_fenetre.renderer, 0, 0, 0, 255); // on met le fond en noir
        SDL_RenderClear(ma_fenetre.renderer); // on efface le rendu

        // DESSIN DU RECTANGLE
        if (deplacementRect)
        {
            // si on touche le bord droit
            if (xPosRect >= ma_fenetre.width - RECT_W)
                directionRectW = -10;

            // si on touche le bord gauche
            if (xPosRect <= 0)
                directionRectW = 10;

            // si on touche le bord bas
            if (yPosRect >= ma_fenetre.height - RECT_W)
                directionRectH = -10;

            // si on touche le bord haut
            if (yPosRect <= 0)
                directionRectH = 10;

            // on déplace le rectangle
            xPosRect += directionRectW;
            yPosRect += directionRectH;
        }

        // si on doit changer la couleur du rectangle
        if (changeCouleurRect)
        {
            couleurRectR = rand() % 256;
            couleurRectG = rand() % 256;
            couleurRectB = rand() % 256;
        }

        dessinerRectangle(ma_fenetre.renderer, xPosRect, yPosRect, couleurRectR, couleurRectG, couleurRectB);

        SDL_RenderPresent(ma_fenetre.renderer); // On affiche le rendu

        SDL_Delay(50); // Petite pause
    }

    SDL_DestroyRenderer(ma_fenetre.renderer);
    SDL_DestroyWindow(ma_fenetre.window);
    SDL_Quit();
}