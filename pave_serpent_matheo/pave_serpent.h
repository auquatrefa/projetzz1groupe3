#ifndef pave_serpent_h
#define pave_serpent_h

#include <SDL2/SDL.h>

typedef struct donnees_fenetre_s
{
    SDL_Window *window;
    SDL_Renderer *renderer;
    int width;
    int height;
} donnees_f_t;


void afficheFenetre();

#endif