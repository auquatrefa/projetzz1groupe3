#ifndef minmax_h
#define minmax_h

#include "jeu.h"

typedef struct liste_coups_s
{
    int x_origine;
    int y_origine;
    int x_fin;
    int y_fin;
    struct liste_coups_s * suiv;
} liste_coups_t;

#include <stdio.h>
#include <stdlib.h>

#include "minmax.h"

#define PROF_MAX 3

/*********************************************************************/
/*                                                                   */
/*                          genereCoups                              */
/*                                                                   */
/*********************************************************************/

liste_coups_t *creerListeCoups();
liste_coups_t *creerCoup(int x_origine, int y_origine, int x_fin, int y_fin);
void ajouterCoup(liste_coups_t *liste, liste_coups_t *coup);
void afficheCoup(liste_coups_t *coup);
void afficheListeCoups(liste_coups_t *liste);
void libererListeCoups(liste_coups_t *liste);
int estPossible(case_t c);
int possedeMarqueur(case_t c);
void genereCoupAnneauHaut(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles);
void genereCoupAnneauDroite(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles);
void genereCoupAnneauBasGauche(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles);
void genereCoupAnneauBas(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles);
void genereCoupAnneauGauche(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles);
void genereCoupAnneauHautDroite(case_t ** grille_jeu, int x, int y, liste_coups_t *coups_possibles);
void genereCoupAnneau(case_t ** grille_jeu, int x, int y, liste_coups_t *coups_possibles);
liste_coups_t *genereCoups(jeu * mon_jeu, int joueur);

/*********************************************************************/
/*                                                                   */
/*                       evaluationPosition                          */
/*                                                                   */
/*********************************************************************/

int estPositionTerminale(jeu * mon_jeu);
int calculScorePositionAvecAnneaux(jeu * mon_jeu, int joueur);
int calculScorePositionSansAnneaux(jeu * mon_jeu, int joueur);
int evaluationMAX(jeu * mon_jeu, int ite, int joueur, int (*calculScorePosition)(jeu *, int));
int evaluationMIN(jeu * mon_jeu, int ite, int joueur, int (*calculScorePosition)(jeu *, int));
int evaluationPosition(jeu * mon_jeu, int joueur, int (*calculScorePosition)(jeu *, int));

/*********************************************************************/
/*                                                                   */
/*                            choisirCoup                            */
/*                                                                   */
/*********************************************************************/

void choisirCoup(jeu * mon_jeu, int joueur, int (*calculScorePosition)(jeu *, int));

#endif