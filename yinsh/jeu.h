#ifndef jeu_h
#define jeu_h

#define LIGNES 11
#define COLONNES 11

typedef struct case_s
{
    int a_marqueur_blanc;
    int a_marqueur_noir;
    int a_anneau_blanc;
    int a_anneau_noir;
    int est_jouable;
} case_t;

typedef struct jeu
{
    case_t **plateau;
    int **anneaux_noirs;
    int **anneaux_blancs;
    int nb_marqueurs_noirs;
    int nb_marqueurs_blancs;
} jeu;

jeu *creeJeu();
int placementPossible(jeu *jeu, int x, int y);
jeu *placeAnneau(jeu *jeu, int joueur, int x, int y);
int deplacementPossible(jeu *jeu, int joueur, int x_deb, int y_deb, int x_fin, int y_fin);
jeu *deplaceAnneau(jeu *jeu, int joueur, int x_deb, int y_deb, int x_fin, int y_fin);
jeu *desappliqueDeplaceAnneau(jeu *jeu, int joueur, int x_deb, int y_deb, int x_fin, int y_fin);
int verifieAlignementDirection(jeu *jeu, int joueur, int *alignement);
int *chercheAlignement(jeu *jeu, int joueur);
jeu *retireAlignement(jeu *jeu, int joueur, int *alignement);
jeu *retireAnneau(jeu *jeu, int joueur, int x, int y);
int nbAnneauxGagnes(jeu *jeu, int joueur);
void libereJeu(jeu *jeu);
void afficheGrille(jeu *jeu);

#endif