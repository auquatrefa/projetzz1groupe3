#include <stdio.h>
#include <stdlib.h>

#include "jeu.h"

jeu *creeJeu()
{
    jeu *j = (jeu *)malloc(sizeof(jeu));

    j->plateau = (case_t **)malloc(11 * sizeof(case_t *));

    for (int i = 0; i < 11; ++i)
    {
        j->plateau[i] = (case_t *)malloc(11 * sizeof(case_t));

        for (int k = 0; k < 11; ++k)
        {
            j->plateau[i][k].a_anneau_blanc = 0;
            j->plateau[i][k].a_anneau_noir = 0;
            j->plateau[i][k].a_marqueur_blanc = 0;
            j->plateau[i][k].a_marqueur_noir = 0;
            if (i == 0)
            {
                if (k >= 6 && k <= 9)
                {
                    j->plateau[i][k].est_jouable = 1;
                }
                else
                {
                    j->plateau[i][k].est_jouable = 0;
                }
            }
            else if (i == 10)
            {
                if (k >= 1 && k <= 4)
                {
                    j->plateau[i][k].est_jouable = 1;
                }
                else
                {
                    j->plateau[i][k].est_jouable = 0;
                }
            }
            else if (i == 5)
            {
                if (k >= 1 && k <= 9)
                {
                    j->plateau[i][k].est_jouable = 1;
                }
                else
                {
                    j->plateau[i][k].est_jouable = 0;
                }
            }
            else if (i >= 1 && i <= 4)
            {
                if (k >= 5 - i && k <= 10)
                {
                    j->plateau[i][k].est_jouable = 1;
                }
                else
                {
                    j->plateau[i][k].est_jouable = 0;
                }
            }
            else
            {
                if (k <= 15 - i)
                {
                    j->plateau[i][k].est_jouable = 1;
                }
                else
                {
                    j->plateau[i][k].est_jouable = 0;
                }
            }
        }
    }

    j->anneaux_blancs = (int **)malloc(5 * sizeof(int *));
    j->anneaux_noirs = (int **)malloc(5 * sizeof(int *));
    for (int i = 0; i < 5; ++i)
    {
        j->anneaux_blancs[i] = (int *)malloc(2 * sizeof(int));
        j->anneaux_noirs[i] = (int *)malloc(2 * sizeof(int));
        j->anneaux_blancs[i][0] = -1;
        j->anneaux_blancs[i][1] = -1;
        j->anneaux_noirs[i][0] = -1;
        j->anneaux_noirs[i][1] = -1;
    }
    j->nb_marqueurs_blancs = 0;
    j->nb_marqueurs_noirs = 0;

    return j;
}

int placementPossible(jeu *jeu, int x, int y)
{
    return (jeu->plateau[x][y].est_jouable && !jeu->plateau[x][y].a_anneau_blanc) && !jeu->plateau[x][y].a_anneau_noir;
}

jeu *placeAnneau(jeu *jeu, int joueur, int x, int y)
{
    int a = 0;
    if (joueur)
    {
        jeu->plateau[x][y].a_anneau_blanc = 1;
        while (a < 5 && jeu->anneaux_blancs[a][0] != -1 && jeu->anneaux_blancs[a][1] != -1)
        {
            a++;
        }
        if (a != 5)
        {
            jeu->anneaux_blancs[a][0] = x;
            jeu->anneaux_blancs[a][1] = y;
        }
    }
    else
    {
        jeu->plateau[x][y].a_anneau_noir = 1;
        while (a < 5 && jeu->anneaux_noirs[a][0] != -1 && jeu->anneaux_noirs[a][1] != -1)
        {
            a++;
        }
        if (a != 5)
        {
            jeu->anneaux_noirs[a][0] = x;
            jeu->anneaux_noirs[a][1] = y;
        }
    }

    return jeu;
}

int deplacementPossible(jeu *jeu, int joueur, int x_deb, int y_deb, int x_fin, int y_fin)
{
    if (placementPossible(jeu, x_fin, y_fin) &&
        ((joueur && jeu->plateau[x_deb][y_deb].a_anneau_blanc) || (!joueur && jeu->plateau[x_deb][y_deb].a_anneau_noir)))
    {
        if (!jeu->plateau[x_fin][y_fin].a_marqueur_blanc && !jeu->plateau[x_fin][y_fin].a_marqueur_noir)
        {
            int marqueur = 0, k = 0;
            if (x_deb == x_fin)
            {
                if (y_deb < y_fin)
                {
                    k = 1;
                }
                else
                {
                    k = -1;
                }

                for (int i = y_deb + k; i != y_fin; i += k)
                {
                    if (jeu->plateau[x_fin][i].a_anneau_blanc || jeu->plateau[x_fin][i].a_anneau_noir)
                    {
                        return 0;
                    }
                    else if (jeu->plateau[x_fin][i].a_marqueur_blanc || jeu->plateau[x_fin][i].a_marqueur_noir)
                    {
                        marqueur = 1;
                    }
                    else
                    {
                        if (marqueur)
                        {
                            return 0;
                        }
                    }
                }
                return 1;
            }
            else if (y_deb == y_fin)
            {
                if (x_deb < x_fin)
                {
                    k = 1;
                }
                else
                {
                    k = -1;
                }

                for (int i = x_deb + k; i != x_fin; i += k)
                {
                    if (jeu->plateau[i][y_fin].a_anneau_blanc || jeu->plateau[i][y_fin].a_anneau_noir)
                    {
                        return 0;
                    }
                    else if (jeu->plateau[i][y_fin].a_marqueur_blanc || jeu->plateau[i][y_fin].a_marqueur_noir)
                    {
                        marqueur = 1;
                    }
                    else
                    {
                        if (marqueur)
                        {
                            return 0;
                        }
                    }
                }
                return 1;
            }
            else if (x_deb - x_fin == y_fin - y_deb)
            {
                if (x_deb < x_fin)
                {
                    k = 1;
                }
                else
                {
                    k = -1;
                }

                for (int i = k; i != x_fin - x_deb; i += k)
                {
                    if (jeu->plateau[x_deb + i][y_deb - i].a_anneau_blanc || jeu->plateau[x_deb + i][y_deb - i].a_anneau_noir)
                    {
                        return 0;
                    }
                    else if (jeu->plateau[x_deb + i][y_deb - i].a_marqueur_blanc || jeu->plateau[x_deb + i][y_deb - i].a_marqueur_noir)
                    {
                        marqueur = 1;
                    }
                    else
                    {
                        if (marqueur)
                        {
                            return 0;
                        }
                    }
                }
                return 1;
            }
        }
    }

    return 0;
}

jeu *deplaceAnneau(jeu *jeu, int joueur, int x_deb, int y_deb, int x_fin, int y_fin)
{
    int a = 0;
    if (joueur)
    {
        while (a < 5 && (jeu->anneaux_blancs[a][0] != x_deb || jeu->anneaux_blancs[a][1] != y_deb))
        {
            a++;
        }
        jeu->plateau[x_deb][y_deb].a_anneau_blanc = 0;
        jeu->plateau[x_fin][y_fin].a_anneau_blanc = 1;
        jeu->anneaux_blancs[a][0] = x_fin;
        jeu->anneaux_blancs[a][1] = y_fin;
        jeu->plateau[x_deb][y_deb].a_marqueur_blanc = 1;
        jeu->nb_marqueurs_blancs++;
    }
    else
    {
        while (a < 5 && (jeu->anneaux_noirs[a][0] != x_deb || jeu->anneaux_noirs[a][1] != y_deb))
        {
            a++;
        }
        jeu->plateau[x_deb][y_deb].a_anneau_noir = 0;
        jeu->plateau[x_fin][y_fin].a_anneau_noir = 1;
        jeu->anneaux_noirs[a][0] = x_fin;
        jeu->anneaux_noirs[a][1] = y_fin;
        jeu->plateau[x_deb][y_deb].a_marqueur_noir = 1;
        jeu->nb_marqueurs_noirs++;
    }

    int k = 0;
    if (x_deb == x_fin)
    {
        if (y_deb < y_fin)
        {
            k = 1;
        }
        else
        {
            k = -1;
        }

        for (int i = y_deb + k; i != y_fin; i += k)
        {
            if (jeu->plateau[x_fin][i].a_marqueur_blanc)
            {
                jeu->plateau[x_fin][i].a_marqueur_blanc = 0;
                jeu->plateau[x_fin][i].a_marqueur_noir = 1;
                jeu->nb_marqueurs_blancs--;
                jeu->nb_marqueurs_noirs++;
            }
            else if (jeu->plateau[x_fin][i].a_marqueur_noir)
            {
                jeu->plateau[x_fin][i].a_marqueur_noir = 0;
                jeu->plateau[x_fin][i].a_marqueur_blanc = 1;
                jeu->nb_marqueurs_blancs++;
                jeu->nb_marqueurs_noirs--;
            }
        }
    }
    else if (y_deb == y_fin)
    {
        if (x_deb < x_fin)
        {
            k = 1;
        }
        else
        {
            k = -1;
        }

        for (int i = x_deb + k; i != x_fin; i += k)
        {
            if (jeu->plateau[i][y_fin].a_marqueur_blanc)
            {
                jeu->plateau[i][y_fin].a_marqueur_blanc = 0;
                jeu->plateau[i][y_fin].a_marqueur_noir = 1;
                jeu->nb_marqueurs_blancs--;
                jeu->nb_marqueurs_noirs++;
            }
            else if (jeu->plateau[i][y_fin].a_marqueur_noir)
            {
                jeu->plateau[i][y_fin].a_marqueur_noir = 0;
                jeu->plateau[i][y_fin].a_marqueur_blanc = 1;
                jeu->nb_marqueurs_blancs++;
                jeu->nb_marqueurs_noirs--;
            }
        }
    }
    else if (x_deb - x_fin == y_fin - y_deb)
    {
        if (x_deb < x_fin)
        {
            k = 1;
        }
        else
        {
            k = -1;
        }

        for (int i = k; i != x_fin - x_deb; i += k)
        {
            if (jeu->plateau[x_deb + i][y_deb - i].a_marqueur_blanc)
            {
                jeu->plateau[x_deb + i][y_deb - i].a_marqueur_blanc = 0;
                jeu->plateau[x_deb + i][y_deb - i].a_marqueur_noir = 1;
                jeu->nb_marqueurs_blancs--;
                jeu->nb_marqueurs_noirs++;
            }
            else if (jeu->plateau[x_deb + i][y_deb - i].a_marqueur_noir)
            {
                jeu->plateau[x_deb + i][y_deb - i].a_marqueur_noir = 0;
                jeu->plateau[x_deb + i][y_deb - i].a_marqueur_blanc = 1;
                jeu->nb_marqueurs_blancs++;
                jeu->nb_marqueurs_noirs--;
            }
        }
    }

    return jeu;
}

jeu *desappliqueDeplaceAnneau(jeu *jeu, int joueur, int x_deb, int y_deb, int x_fin, int y_fin)
{
    int a = 0;
    if (joueur)
    {
        while (a < 5 && (jeu->anneaux_blancs[a][0] != x_fin || jeu->anneaux_blancs[a][1] != y_fin))
        {
            a++;
        }
        jeu->plateau[x_deb][y_deb].a_anneau_blanc = 1;
        jeu->plateau[x_fin][y_fin].a_anneau_blanc = 0;
        jeu->anneaux_blancs[a][0] = x_deb;
        jeu->anneaux_blancs[a][1] = y_deb;
        jeu->plateau[x_deb][y_deb].a_marqueur_blanc = 0;
        jeu->nb_marqueurs_blancs--;
    }
    else
    {
        while (a < 5 && (jeu->anneaux_noirs[a][0] != x_fin || jeu->anneaux_noirs[a][1] != y_fin))
        {
            a++;
        }
        jeu->plateau[x_deb][y_deb].a_anneau_noir = 1;
        jeu->plateau[x_fin][y_fin].a_anneau_noir = 0;
        jeu->anneaux_noirs[a][0] = x_deb;
        jeu->anneaux_noirs[a][1] = y_deb;
        jeu->plateau[x_deb][y_deb].a_marqueur_noir = 0;
        jeu->nb_marqueurs_noirs--;
    }

    int k = 0;
    if (x_deb == x_fin)
    {
        if (y_deb < y_fin)
        {
            k = -1;
        }
        else
        {
            k = 1;
        }

        for (int i = y_fin + k; i != y_deb; i += k)
        {
            if (jeu->plateau[x_fin][i].a_marqueur_blanc)
            {
                jeu->plateau[x_fin][i].a_marqueur_blanc = 0;
                jeu->plateau[x_fin][i].a_marqueur_noir = 1;
                jeu->nb_marqueurs_blancs--;
                jeu->nb_marqueurs_noirs++;
            }
            else if (jeu->plateau[x_fin][i].a_marqueur_noir)
            {
                jeu->plateau[x_fin][i].a_marqueur_noir = 0;
                jeu->plateau[x_fin][i].a_marqueur_blanc = 1;
                jeu->nb_marqueurs_blancs++;
                jeu->nb_marqueurs_noirs--;
            }
        }
    }
    else if (y_deb == y_fin)
    {
        if (x_deb < x_fin)
        {
            k = -1;
        }
        else
        {
            k = 1;
        }

        for (int i = x_fin + k; i != x_deb; i += k)
        {
            if (jeu->plateau[i][y_fin].a_marqueur_blanc)
            {
                jeu->plateau[i][y_fin].a_marqueur_blanc = 0;
                jeu->plateau[i][y_fin].a_marqueur_noir = 1;
                jeu->nb_marqueurs_blancs--;
                jeu->nb_marqueurs_noirs++;
            }
            else if (jeu->plateau[i][y_fin].a_marqueur_noir)
            {
                jeu->plateau[i][y_fin].a_marqueur_noir = 0;
                jeu->plateau[i][y_fin].a_marqueur_blanc = 1;
                jeu->nb_marqueurs_blancs++;
                jeu->nb_marqueurs_noirs--;
            }
        }
    }
    else if (x_deb - x_fin == y_fin - y_deb)
    {
        if (x_deb < x_fin)
        {
            k = -1;
        }
        else
        {
            k = 1;
        }

        for (int i = k; i != x_deb - x_fin; i += k)
        {
            if (jeu->plateau[x_fin + i][y_fin - i].a_marqueur_blanc)
            {
                jeu->plateau[x_fin + i][y_fin - i].a_marqueur_blanc = 0;
                jeu->plateau[x_fin + i][y_fin - i].a_marqueur_noir = 1;
                jeu->nb_marqueurs_blancs--;
                jeu->nb_marqueurs_noirs++;
            }
            else if (jeu->plateau[x_fin + i][y_fin - i].a_marqueur_noir)
            {
                jeu->plateau[x_fin + i][y_fin - i].a_marqueur_noir = 0;
                jeu->plateau[x_fin + i][y_fin - i].a_marqueur_blanc = 1;
                jeu->nb_marqueurs_blancs++;
                jeu->nb_marqueurs_noirs--;
            }
        }
    }

    return jeu;
}

int verifieAlignementDirection(jeu *jeu, int joueur, int *alignement)
{
    //dir : 0 (est), 1 (sud), 2 (diag)
    int compteur = 1;
    if (alignement[2] == 0 && alignement[1] + 4 < 11 && jeu->plateau[alignement[0]][alignement[1] + 4].est_jouable)
    {
        if (joueur)
        {
            while (jeu->plateau[alignement[0]][alignement[1] + compteur].a_marqueur_blanc && compteur < 5)
            {
                compteur++;
            }
        }
        else
        {
            while (jeu->plateau[alignement[0]][alignement[1] + compteur].a_marqueur_noir && compteur < 5)
            {
                compteur++;
            }
        }
    }
    else if (alignement[2] == 1 && alignement[0] + 4 < 11 && jeu->plateau[alignement[0] + 4][alignement[1]].est_jouable)
    {
        if (joueur)
        {
            while (jeu->plateau[alignement[0] + compteur][alignement[1]].a_marqueur_blanc && compteur < 5)
            {
                compteur++;
            }
        }
        else
        {
            while (jeu->plateau[alignement[0] + compteur][alignement[1]].a_marqueur_noir && compteur < 5)
            {
                compteur++;
            }
        }
    }
    else if (alignement[2] == 2 && (alignement[0] + 4 < 11 && alignement[1] - 4 >= 0) && jeu->plateau[alignement[0] + 4][alignement[1] - 4].est_jouable)
    {
        if (joueur)
        {
            while (jeu->plateau[alignement[0] + compteur][alignement[1] - compteur].a_marqueur_blanc && compteur < 5)
            {
                compteur++;
            }
        }
        else
        {
            while (jeu->plateau[alignement[0] + compteur][alignement[1] - compteur].a_marqueur_noir && compteur < 5)
            {
                compteur++;
            }
        }
    }

    if (compteur != 5)
    {
        return 0;
    }
    return 1;
}

int *chercheAlignement(jeu *jeu, int joueur)
{
    int *alignement = (int *)malloc(3 * sizeof(int));
    for (int i = 0; i < 11; ++i)
    {
        for (int j = 0; j < 11; ++j)
        {
            if ((jeu->plateau[i][j].a_marqueur_blanc && joueur) || (jeu->plateau[i][j].a_marqueur_noir && !joueur))
            {
                alignement[0] = i;
                alignement[1] = j;
                alignement[2] = 0;
                while (alignement[2] < 3)
                {
                    if (verifieAlignementDirection(jeu, joueur, alignement))
                    {
                        return alignement;
                    }
                    alignement[2]++;
                }
            }
        }
    }
    return NULL;
}

jeu *retireAlignement(jeu *jeu, int joueur, int *alignement)
{
    int i = alignement[0], j = alignement[1];
    if ((jeu->plateau[i][j].a_marqueur_blanc && joueur) || (jeu->plateau[i][j].a_marqueur_noir && !joueur))
    {
        int k = 0;
        while (k < 5)
        {
            if (joueur)
            {
                jeu->plateau[i][j].a_marqueur_blanc = 0;
                jeu->nb_marqueurs_blancs--;
            }
            else
            {
                jeu->plateau[i][j].a_marqueur_noir = 0;
                jeu->nb_marqueurs_noirs--;
            }

            if (alignement[2] == 1 || alignement[2] == 2)
            {
                i++;
            }
            if (alignement[2] == 0)
            {
                j++;
            }
            if (alignement[2] == 2)
            {
                j--;
            }
            k++;
        }
    }
    return jeu;
}

jeu *retireAnneau(jeu *jeu, int joueur, int x, int y)
{
    if ((jeu->plateau[x][y].a_anneau_blanc && joueur) || (jeu->plateau[x][y].a_anneau_noir && !joueur))
    {
        int a = 0;
        if (joueur)
        {
            while (a < 5 && (jeu->anneaux_blancs[a][0] != x || jeu->anneaux_blancs[a][1] != y))
            {
                a++;
            }
            if (a != 5)
            {
                jeu->plateau[x][y].a_anneau_blanc = 0;
                jeu->anneaux_blancs[a][0] = -1;
                jeu->anneaux_blancs[a][1] = -1;
            }
        }
        else
        {
            while (a < 5 && (jeu->anneaux_noirs[a][0] != x || jeu->anneaux_noirs[a][1] != y))
            {
                a++;
            }
            if (a != 5)
            {
                jeu->plateau[x][y].a_anneau_noir = 0;
                jeu->anneaux_noirs[a][0] = -1;
                jeu->anneaux_noirs[a][1] = -1;
            }
        }
    }
    return jeu;
}

int nbAnneauxGagnes(jeu *jeu, int joueur)
{
    int compteur = 0, i = 0;
    while (i < 5)
    {
        if ((joueur && jeu->anneaux_blancs[i][0] == -1) || (!joueur && jeu->anneaux_noirs[i][0] == -1))
        {
            compteur++;
        }
        i++;
    }

    return compteur;
}

void libereJeu(jeu *jeu)
{
    for (int i = 0; i < 11; ++i)
    {
        free(jeu->plateau[i]);
    }
    free(jeu->plateau);

    for (int i = 1; i < 5; ++i)
    {
        free(jeu->anneaux_blancs[i]);
        free(jeu->anneaux_noirs[i]);
    }
    free(jeu->anneaux_blancs);
    free(jeu->anneaux_noirs);
    free(jeu);
}

void afficheGrille(jeu *jeu)
{
    for (int i = 0; i < 11; i++) {
        for (int j = 0; j < 11; j++) {
            if (!jeu->plateau[i][j].est_jouable) {
                printf(" ");
            } else if (jeu->plateau[i][j].a_anneau_blanc) {
                printf("o");
            } else if (jeu->plateau[i][j].a_anneau_noir) {
                printf("c");
            } else if (jeu->plateau[i][j].a_marqueur_blanc) {
                printf(".");
            } else if (jeu->plateau[i][j].a_marqueur_noir) {
                printf(",");
            } else {
                printf("x");
            }
        }
        printf("\n");
    }
}