#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "affichSDL.h"
#include "jeu.h"
#include "minmax.h"

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    SDL_bool tourne = SDL_TRUE;

    jeu *mon_jeu = creeJeu();
    mon_jeu = placeAnneau(mon_jeu, 0, 2, 6);
    mon_jeu = placeAnneau(mon_jeu, 1, 3, 6);
    mon_jeu = placeAnneau(mon_jeu, 0, 4, 5);
    mon_jeu = placeAnneau(mon_jeu, 1, 5, 3);
    mon_jeu = placeAnneau(mon_jeu, 0, 3, 9);
    mon_jeu = placeAnneau(mon_jeu, 1, 5, 7);
    mon_jeu = placeAnneau(mon_jeu, 0, 9, 3);
    mon_jeu = placeAnneau(mon_jeu, 1, 9, 1);
    mon_jeu = placeAnneau(mon_jeu, 0, 10, 1);
    mon_jeu = placeAnneau(mon_jeu, 1, 9, 2);

    FILE *f;
    //int statut = 1;
    int x_deb = 0, y_deb = 0, x_fin = 0, y_fin = 0;
    int tour = 1;
    initFenetre();
    int* alignement = NULL;
    int points[2] = {0, 0};
    while (tourne)
    {
        // afficheGrille(mon_jeu);
        afficheFenetre(mon_jeu);
        if (tour % 2)
        {
            // printf("\nPoints :");
            // scanf("%d,%d;%d,%d", &x_deb, &y_deb, &x_fin, &y_fin);
            // statut = init();
            // if (!statut)
            // {
            //     end_sdl(0, "Fenêtre fermée");
            //     exit(EXIT_FAILURE);
            // }
            // f = fopen("coup.txt", "r");
            // fscanf(f, "%d,%d", &x_deb, &y_deb);
            // fclose(f);
            // statut = init();
            // if (!statut)
            // {
            //     end_sdl(0, "Fenêtre fermée");
            //     exit(EXIT_FAILURE);
            // }
            // f = fopen("coup.txt", "r");
            // fscanf(f, "%d,%d", &x_fin, &y_fin);
            // fclose(f);
            choisirCoup(mon_jeu, tour%2, calculScorePositionAvecAnneaux);
            f = fopen("coup.txt", "r");
            fscanf(f, "%d,%d;%d,%d", &x_deb, &y_deb, &x_fin, &y_fin);
            fclose(f);
        }
        else
        {
            choisirCoup(mon_jeu, tour%2, calculScorePositionSansAnneaux);
            f = fopen("coup.txt", "r");
            fscanf(f, "%d,%d;%d,%d", &x_deb, &y_deb, &x_fin, &y_fin);
            fclose(f);
        }

        if (deplacementPossible(mon_jeu, tour % 2, x_deb, y_deb, x_fin, y_fin))
        {
            mon_jeu = deplaceAnneau(mon_jeu, tour % 2, x_deb, y_deb, x_fin, y_fin);
            alignement = chercheAlignement(mon_jeu, tour % 2);
            if (alignement != NULL) {
                mon_jeu = retireAlignement(mon_jeu, tour % 2, alignement);
                mon_jeu = retireAnneau(mon_jeu, tour % 2, x_fin, y_fin);
                points[tour % 2] ++;
                free(alignement);
            }
            tour++;
        }
        sleep(1);

        if (points[0] > 2 || points[1] > 2) {
            tourne = SDL_FALSE;
        }
    }

    sleep(15);

    libereJeu(mon_jeu);
    end_sdl(1, "VICTOIRE");
    return 0;
}