#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "affichSDL.h"
#include "jeu.h"

#define LIGNES 11
#define COLONNES 11

/* VARIABLES GLOBALES */
SDL_Texture *ma_texture;  // Texture contenant le fond
SDL_Texture *ma_texture2; // Texture contenant le jeton à afficher
donnees_f_t ma_fenetre;   // Structure contenant les informations sur la fenêtre
SDL_DisplayMode mode;
// textures représentant les différents jetons
SDL_Texture *anneauBlanc;
SDL_Texture *anneauNoir;
SDL_Texture *marqueurBlanc;
SDL_Texture *marqueurNoir;

/*****************************************************************************************/
/* Fonction end_sdl                                                                      */
/*                                                                                       */
/* Cette fonction permet de quitter la SDL en cas d'erreur                               */
/*                                                                                       */
/* Paramètres :                                                                          */
/*      - char ok : 0 si tout s'est bien passé, 1 sinon                                  */
/*      - char const *msg : message à afficher en cas d'erreur                           */
/*      - SDL_Window *window : fenêtre à fermer                                          */
/*      - SDL_Renderer *renderer : renderer à fermer                                     */
/*                                                                                       */
/* Retour : void                                                                         */
/*                                                                                       */
/*****************************************************************************************/
void end_sdl(char ok, // fin normale : ok = 0 ; anormale ok = 1
             char const *msg)
{ // message à afficher
    char msg_formate[255];
    int l;

    if (!ok)
    { // Affichage de ce qui ne va pas
        strncpy(msg_formate, msg, 250);
        l = strlen(msg_formate);
        strcpy(msg_formate + l, " : %s\n");

        SDL_Log(msg_formate, SDL_GetError());
    }

    if (ma_fenetre.rendu != NULL)
    {                                          // Destruction si nécessaire du renderer
        SDL_DestroyRenderer(ma_fenetre.rendu); // Attention : on suppose que les NULL sont maintenus !!
        ma_fenetre.rendu = NULL;
    }
    if (ma_fenetre.fenetre != NULL)
    {                                          // Destruction si nécessaire de la fenêtre
        SDL_DestroyWindow(ma_fenetre.fenetre); // Attention : on suppose que les NULL sont maintenus !!
        ma_fenetre.fenetre = NULL;
    }

    SDL_Quit();

    if (!ok)
    { // On quitte si cela ne va pas
        exit(EXIT_FAILURE);
    }
}

/*********************************************************************************/
/* Fonction initFenetre                                                          */
/*                                                                               */
/* Fonction initialisant tout ce qui faut pour créer une fenêtre (à appeler dans */
/*   le main une fois au début du jeu)                                           */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void initFenetre()
{
    ma_fenetre.fenetre = NULL;
    ma_fenetre.rendu = NULL;

    /* Création de la SDL */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        end_sdl(0, "ERROR SDL INIT");

    SDL_GetCurrentDisplayMode(0, &mode); // données de l'ecran

    /* Création de la fenêtre */
    ma_fenetre.fenetre = SDL_CreateWindow("Jeu de yinsh", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1025, 1025, SDL_WINDOW_OPENGL);
    if (ma_fenetre.fenetre == NULL)
        end_sdl(0, "ERROR WINDOW CREATION");

    /* Création du renderer */
    ma_fenetre.rendu = SDL_CreateRenderer(ma_fenetre.fenetre, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (ma_fenetre.rendu == NULL)
        end_sdl(0, "ERROR RENDERER CREATION");

    /* Chargement du rendu contenant le fond de la fenêtre */
    ma_texture = IMG_LoadTexture(ma_fenetre.rendu, "./images/plateauJeu.png");
    if (ma_texture == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture");
}

/*********************************************************************************/
/* Fonction initJetons                                                           */
/*                                                                               */
/* Fonction initialisant tout ce qui faut pour créer les jetons (à appeler dans  */
/*   le main une fois au début du jeu)                                           */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void initJetons()
{
    /* Création des textures */
    anneauBlanc = IMG_LoadTexture(ma_fenetre.rendu, "./images/anneauBlanc.png");
    if (anneauBlanc == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture");
    anneauNoir = IMG_LoadTexture(ma_fenetre.rendu, "./images/anneauNoir.png");
    if (anneauNoir == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture");
    marqueurBlanc = IMG_LoadTexture(ma_fenetre.rendu, "./images/marqueurBlanc.png");
    if (marqueurBlanc == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture");
    marqueurNoir = IMG_LoadTexture(ma_fenetre.rendu, "./images/marqueurNoir.png");
    if (marqueurNoir == NULL)
        end_sdl(0, "Echec du chargement de l'image dans la texture");
}

/*********************************************************************************/
/* Fonction afficheFenetre                                                       */
/*                                                                               */
/* Fonction permettant d'afficher une fenêtre avec le plateau en fond            */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void afficheFenetre(jeu *mon_jeu)
{

    SDL_RenderClear(ma_fenetre.rendu);

    /* Affichage Plateau */
    //printf("Test\n");
    SDL_RenderCopy(ma_fenetre.rendu, ma_texture, NULL, NULL);

    //SDL_RenderPresent(ma_fenetre.rendu);

    /* Affichage Jetons */
    // BOUCLE PARCOURANT LE PLATEAU ET POUR CHAQUE JETON QU'ELLE CROISE l'AFFICHE
    // (void) plateau;
    initJetons();
    for (int i = 0; i < LIGNES; ++i)
    {
        // printf("i= %d\n",i);
        for (int j = 0; j < COLONNES; ++j)
        {
            // printf("j= %d\n",j);
            if (mon_jeu->plateau[i][j].est_jouable)
            {
                // printf("i= %d, j= %d\n",i,j);
                SDL_Rect destination = {0};
                // Reconversion en coord
                // destination.x = i*80+77;
                // destination.y = abs((-1*(6-2*j)+(i-77)/80)*50+50);
                destination.x = 45 + 88.5 * j;          //((i+1)*50)+30;
                destination.y = 101 * i + 50 * j - 265; //(70 + 80*j)-25;
                destination.w = 50;
                destination.h = 50;
                //printf("------%d,%d\n",destination.x,destination.y);
                if (mon_jeu->plateau[i][j].a_anneau_blanc)
                {
                    //puts("Test3");
                    SDL_RenderCopy(ma_fenetre.rendu, anneauBlanc, NULL, &destination);
                }
                else
                {
                    if (mon_jeu->plateau[i][j].a_anneau_noir)
                    {
                        //printf("%d %d %d %d\n",destination.x, destination.y, destination.w, destination.h);
                        // destination.x = 25;
                        // destination.y = 25;
                        //puts("Test4");
                        SDL_RenderCopy(ma_fenetre.rendu, anneauNoir, NULL, &destination);
                    }
                }
                if (mon_jeu->plateau[i][j].a_marqueur_blanc)
                {
                    //puts("Test1");
                    SDL_RenderCopy(ma_fenetre.rendu, marqueurBlanc, NULL, &destination);
                }
                else
                {
                    if (mon_jeu->plateau[i][j].a_marqueur_noir)
                    {
                        //puts("Test2");
                        SDL_RenderCopy(ma_fenetre.rendu, marqueurNoir, NULL, &destination);
                    }
                }
            }
        }
    }

    // SDL_Rect destination = {0,0,100,100};

    // SDL_RenderCopy(ma_fenetre.rendu, anneauNoir, NULL, &destination);

    SDL_RenderPresent(ma_fenetre.rendu);
    //SDL_Delay(100);
    // SDL_RenderClear(ma_fenetre.rendu);
}

/*********************************************************************************/
/* Fonction entrees                                                              */
/*                                                                               */
/* Fonction initialisant tout ce qui faut pour créer une fenêtre (à appeler dans)*/
/*   le main une fois au début du jeu                                            */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : Fichier contenant les coordonnées et déplacements à effectuer        */
/*                                                                               */
/*********************************************************************************/
FILE *entrees()
{
    return NULL;
}

void rendu()
{
}

int normalise(int y)
{
    int y_norme = y;
    switch (y_norme)
    {
    case 6:
        y_norme = 0;
        break;
    case 7:
        y_norme = 0;
        break;
    case 4:
        y_norme = 1;
        break;
    case 5:
        y_norme = 1;
        break;
    case 2:
        y_norme = 2;
        break;
    case 3:
        y_norme = 2;
        break;
    case 0:
        y_norme = 3;
        break;

    case 1:
        y_norme = 3;
        break;
    case -2:
        y_norme = 4;
        break;
    case -1:
        y_norme = 4;
        break;
    case -3:
        y_norme = 5;
        break;
    case -4:
        y_norme = 5;
        break;
    case -6:
        y_norme = 6;
        break;
    case -7:
        y_norme = 6;
        break;
    case -9:
        y_norme = 7;
        break;
    case -8:
        y_norme = 7;
        break;
    case -10:
        y_norme = 8;
        break;
    case -11:
        y_norme = 8;
        break;
    case -13:
        y_norme = 9;
        break;
    case -12:
        y_norme = 9;
        break;
    case -15:
        y_norme = 10;
        break;
    case -14:
        y_norme = 10;
        break;
    }
    return y_norme;
}

/*********************************************************************************/
/* Fonction init                                                                 */
/*                                                                               */
/* Fonction gerant l'initialisation du début de partie avec placement des jetons */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
int init()
{
    SDL_Event evenement;
    int x, y, x_norme, y_norme;

    FILE *f = fopen("coup.txt", "w");
    SDL_bool event_utile = SDL_FALSE;

    // Vérif fichier
    // if(!f){
    //     puts("Fichier inutilisable");
    //     return f;
    // }

    while (!event_utile)
    {
        if (SDL_PollEvent(&evenement))
        {
            switch (evenement.type)
            {
            case SDL_WINDOWEVENT:
                switch (evenement.window.event)
                {
                case SDL_WINDOWEVENT_CLOSE:
                    puts("Appui sur la croix");
                    // A traiter dans le main: si le fichier retour est null alors quitter
                    fclose(f);
                    return 0;
                    break;
                default:
                    break;
                }
                break;
            case SDL_QUIT:
                puts("On quitte");
                // A traiter dans le main: si le fichier retour est null alors quitter
                fclose(f);
                return 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                x = evenement.motion.x;
                y = evenement.motion.y;
                x_norme = normalise(((x - 77) / 80) - ((y - 50) / 50));
                y_norme = (x - 77) / 80;

                // printf("%d,%d\n",x_norme,y_norme);
                // printf("%d,%d\n",x,y);
                // (void) f;
                fprintf(f, "%d,%d", x_norme, y_norme);
                event_utile = SDL_TRUE;
                break;
            default:
                break;
            }
        }
    }

    fclose(f);
    return 1;
}

void partie(jeu *j)
{
    FILE *f = NULL;
    initFenetre();
    j->plateau[5][2].a_anneau_noir = 1;
    j->plateau[5][2].a_marqueur_noir = 1;
    puts("testttt");
    while (1)
    {
        afficheFenetre(j);
        init(f);
    }
    // while(tourne){
    //     entrees();
    //     // Romain modifs
    //     rendu();
    // }
    end_sdl(0, "");
}

// SDL_DestroyRenderer(ma_fenetre.rendu);
//     SDL_DestroyWindow(ma_fenetre.fenetre);
//     IMG_Quit();
//     SDL_Quit();
//     SDL_DestroyTexture(ma_texture);
