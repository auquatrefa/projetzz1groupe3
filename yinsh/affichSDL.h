#ifndef affichSDL_h
#define affichSDL_h

#include <SDL2/SDL.h>
#include "jeu.h"

typedef struct donnees_fenetre_s
{
    SDL_Window *fenetre;
    SDL_Renderer *rendu;
    int largeur;
    int hauteur;
} donnees_f_t;

void end_sdl(char ok, char const *msg);
void afficheFenetre(jeu *j);
FILE * entrees();
int init();
void rendu();
void partie(jeu *j);
void initFenetre();
int normalise(int y);

#endif