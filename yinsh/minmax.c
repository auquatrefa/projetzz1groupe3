#include <stdio.h>
#include <stdlib.h>

#include "minmax.h"

/*********************************************************************/
/*                                                                   */
/*                          genereCoups                              */
/*                                                                   */
/*********************************************************************/

/*********************************************************************/
/* creerListeCoups()                                                 */
/*                                                                   */
/* Alloue l'espace memoire pour un coup et met suiv à NULL           */
/*                                                                   */
/* En entree:                                                        */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
liste_coups_t *creerListeCoups()
{
    liste_coups_t *coup = (liste_coups_t *)malloc(sizeof(liste_coups_t));
    coup->x_origine = -1;
    coup->y_origine = -1;
    coup->x_fin = -1;
    coup->y_fin = -1;
    coup->suiv = NULL;

    return coup;
}

/*********************************************************************/
/* creerCoup()                                                       */
/*                                                                   */
/* Alloue l'espace memoire pour un coup et initialise les coordonnées*/
/*                                                                   */
/* En entree:                                                        */
/*      int x_origine : ligne de l'anneau d'origine                  */
/*      int y_origine : colonne de l'anneau d'origine                */
/*      int x_fin : ligne de l'anneau de destination                 */
/*      int y_fin : colonne de l'anneau de destination               */
/*                                                                   */
/* En sortie:                                                        */
/*      liste_coups_t * : coup initialisé                            */
/*                                                                   */
/*********************************************************************/
liste_coups_t *creerCoup(int x_origine, int y_origine, int x_fin, int y_fin)
{
    liste_coups_t *coup = (liste_coups_t *)malloc(sizeof(liste_coups_t));
    coup->x_origine = x_origine;
    coup->y_origine = y_origine;
    coup->x_fin = x_fin;
    coup->y_fin = y_fin;
    coup->suiv = NULL;

    return coup;
}

/*********************************************************************/
/* ajouterCoup()                                                     */
/*                                                                   */
/* Ajoute un coup en tête d'une liste de coups                       */
/*                                                                   */
/* En entree:                                                        */
/*      liste_coups_t * liste : liste de coups                       */
/*      liste_coups_t * coup : coup à ajouter                        */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void ajouterCoup(liste_coups_t *liste, liste_coups_t *coup)
{
    liste_coups_t *tmp = liste->suiv;
    liste->suiv = coup;
    coup->suiv = tmp;
}

/*********************************************************************/
/* afficheCoup()                                                     */
/*                                                                   */
/* Affiche un coup                                                   */
/*                                                                   */
/* En entree:                                                        */
/*      liste_coups_t * coup : coup à afficher                       */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void afficheCoup(liste_coups_t *coup)
{
    printf("Coup : (%d, %d) -> (%d, %d)\n", coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
}

/*********************************************************************/
/* afficheListeCoups()                                               */
/*                                                                   */
/* Affiche une liste de coups                                        */
/*                                                                   */
/* En entree:                                                        */
/*      liste_coups_t * liste : liste de coups                       */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void afficheListeCoups(liste_coups_t *liste)
{
    liste_coups_t *cour = liste->suiv;
    while (cour != NULL)
    {
        afficheCoup(cour);
        cour = cour->suiv;
    }
}

/*********************************************************************/
/* libererListeCoups()                                               */
/*                                                                   */
/* Libère la mémoire d'une liste de coups                            */
/*                                                                   */
/* En entree:                                                        */
/*      liste_coups_t * liste : liste de coups                       */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void libererListeCoups(liste_coups_t *liste)
{
    liste_coups_t *cour = liste->suiv;
    while (cour != NULL)
    {
        liste_coups_t *tmp = cour;
        cour = cour->suiv;
        free(tmp);
    }
}

/*********************************************************************/
/* estPossible()                                                     */
/*                                                                   */
/* Vérifie si une case est jouable                                   */
/*                                                                   */
/* En entree:                                                        */
/*      case_t c : case à vérifier                                   */
/*                                                                   */
/* En sortie:                                                        */
/*      int : 1 si la case est jouable, 0 sinon                      */
/*                                                                   */
/*********************************************************************/
int estPossible(case_t c)
{
    return c.est_jouable && !c.a_anneau_noir && !c.a_anneau_blanc;
}

/*********************************************************************/
/* possedeMarqueur()                                                 */
/*                                                                   */
/* Vérifie si une case possède un marqueur                           */
/*                                                                   */
/* En entree:                                                        */
/*      case_t c : case à vérifier                                   */
/*                                                                   */
/* En sortie:                                                        */
/*      int : 1 si la case possède un marqueur, 0 sinon              */
/*                                                                   */
/*********************************************************************/
int possedeMarqueur(case_t c)
{
    return c.a_marqueur_blanc || c.a_marqueur_noir;
}

/*********************************************************************/
/* genereCoupAnneauHaut()                                            */
/*                                                                   */
/* Génère les coups possibles vers le haut pour un anneau            */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t * coups_possibles : liste des coups possibles  */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneauHaut(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    // coordonnée de la case où il y a l'anneau de base
    int x_init = x+1;
    int y_init = y;

    // Tant qu'on rencontre une case libre
    while (x >= 0                                 // Tant qu'on ne sort pas de la grille,
           && estPossible(grille_jeu[x][y])       // que la case est jouable (pas d'anneau et jouable)
           && !possedeMarqueur(grille_jeu[x][y])) // et que la case ne possède pas de marqueur
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);

        x -= 1; // on avance d'une case en haut
    }

    // Tant qu'on rencontre un marqueur
    while (x >= 0                                // Tant qu'on n'est pas sorti de la grille
           && estPossible(grille_jeu[x][y])      // que la case est jouable (pas d'anneau et jouable)
           && possedeMarqueur(grille_jeu[x][y])) // et que la case possède un marqueur
    {
        x -= 1; // on avance d'une case en haut
    }

    // Si on n'est pas sorti de la grille, qu'on est pas sur un anneau et que la case est jouable
    if (x >= 0 && estPossible(grille_jeu[x][y]))
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);
    }
}

/*********************************************************************/
/* genereCoupAnneauDroite()                                          */
/*                                                                   */
/* Génère les coups possibles vers la droite pour un anneau          */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t * coups_possibles : liste des coups possibles  */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneauDroite(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    // coordonnée de la case où il y a l'anneau de base
    int x_init = x;
    int y_init = y-1;

    // Tant qu'on rencontre une case libre
    while (y < COLONNES                           // Tant qu'on ne sort pas de la grille,
           && estPossible(grille_jeu[x][y])       // que la case est jouable (pas d'anneau et jouable)
           && !possedeMarqueur(grille_jeu[x][y])) // et que la case ne possède pas de marqueur
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);

        y += 1; // on avance d'une case à droite
    }

    // Tant qu'on rencontre un marqueur
    while (y < COLONNES                          // Tant qu'on n'est pas sorti de la grille
           && estPossible(grille_jeu[x][y])      // que la case est jouable (pas d'anneau et jouable)
           && possedeMarqueur(grille_jeu[x][y])) // et que la case possède un marqueur
    {
        y += 1; // on avance d'une case à droite
    }

    // Si on n'est pas sorti de la grille, qu'on est pas sur un anneau et que la case est jouable
    if (y < COLONNES && estPossible(grille_jeu[x][y]))
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);
    }
}

/*********************************************************************/
/* genereCoupAnneauBasGauche()                                       */
/*                                                                   */
/* Génère les coups possibles vers le bas à gauche pour un anneau    */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t * coups_possibles : liste des coups possibles  */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneauBasGauche(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    // coordonnée de la case où il y a l'anneau de base
    int x_init = x-1;
    int y_init = y+1;

    // Tant qu'on rencontre une case libre
    while (x < LIGNES && y >= 0                    // Tant qu'on ne sort pas de la grille,
           && estPossible(grille_jeu[x][y])         // que la case est jouable (pas d'anneau et jouable)
           && !possedeMarqueur(grille_jeu[x][y]))   // et que la case ne possède pas de marqueur
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);

        x += 1; // on avance d'une case en bas
        y -= 1; // on avance d'une case à gauche
    }

    // Tant qu'on rencontre un marqueur
    while (x < LIGNES && y >= 0                   // Tant qu'on n'est pas sorti de la grille
           && estPossible(grille_jeu[x][y])        // que la case est jouable (pas d'anneau et jouable)
           && possedeMarqueur(grille_jeu[x][y]))  // et que la case possède un marqueur
    {
        x += 1; // on avance d'une case en bas
        y -= 1; // on avance d'une case à gauche
    }

    // Si on n'est pas sorti de la grille, qu'on est pas sur un anneau et que la case est jouable
    if (x < LIGNES && y >= 0 && estPossible(grille_jeu[x][y]))
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);
    }
}

/*********************************************************************/
/* genereCoupAnneauBas()                                             */
/*                                                                   */
/* Génère les coups possibles vers le bas pour un anneau             */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t * coups_possibles : liste des coups possibles  */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneauBas(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    // coordonnée de la case où il y a l'anneau de base
    int x_init = x-1;
    int y_init = y;

    // Tant qu'on rencontre une case libre
    while (x < LIGNES                                // Tant qu'on ne sort pas de la grille,
           && estPossible(grille_jeu[x][y])           // que la case est jouable (pas d'anneau et jouable)
           && !possedeMarqueur(grille_jeu[x][y]))     // et que la case ne possède pas de marqueur
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);

        x += 1; // on avance d'une case en bas
    }

    // Tant qu'on rencontre un marqueur
    while (x < LIGNES                               // Tant qu'on n'est pas sorti de la grille
           && estPossible(grille_jeu[x][y])         // que la case est jouable (pas d'anneau et jouable)
           && possedeMarqueur(grille_jeu[x][y]))    // et que la case possède un marqueur
    {
        x += 1; // on avance d'une case en bas
    }

    // Si on n'est pas sorti de la grille, qu'on est pas sur un anneau et que la case est jouable
    if (x < LIGNES && estPossible(grille_jeu[x][y]))
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);
    }
}

/*********************************************************************/
/* genereCoupAnneauGauche()                                          */
/*                                                                   */
/* Génère les coups possibles vers la gauche pour un anneau          */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t * coups_possibles : liste des coups possibles  */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneauGauche(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    // coordonnée de la case où il y a l'anneau de base
    int x_init = x;
    int y_init = y+1;

    // Tant qu'on rencontre une case libre
    while (y >= 0                                 // Tant qu'on ne sort pas de la grille,
           && estPossible(grille_jeu[x][y])       // que la case est jouable (pas d'anneau et jouable)
           && !possedeMarqueur(grille_jeu[x][y])) // et que la case ne possède pas de marqueur
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);

        y -= 1; // on avance d'une case à gauche
    }

    // Tant qu'on rencontre un marqueur
    while (y >= 0                                // Tant qu'on n'est pas sorti de la grille
           && estPossible(grille_jeu[x][y])      // que la case est jouable (pas d'anneau et jouable)
           && possedeMarqueur(grille_jeu[x][y])) // et que la case possède un marqueur
    {
        y -= 1; // on avance d'une case à gauche
    }

    // Si on n'est pas sorti de la grille, qu'on est pas sur un anneau et que la case est jouable
    if (y >= 0 && estPossible(grille_jeu[x][y]))
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);
    }
}

/*********************************************************************/
/* genereCoupAnneauHautDroite()                                      */
/*                                                                   */
/* Génère les coups possibles vers le haut à droite pour un anneau   */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t * coups_possibles : liste des coups possibles  */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneauHautDroite(case_t **grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    // coordonnée de la case où il y a l'anneau de base
    int x_init = x+1;
    int y_init = y-1;

    // Tant qu'on rencontre une case libre
    while (x >= 0 && y < COLONNES                  // Tant qu'on ne sort pas de la grille,
           && estPossible(grille_jeu[x][y])        // que la case est jouable (pas d'anneau et jouable)
           && !possedeMarqueur(grille_jeu[x][y]))  // et que la case ne possède pas de marqueur
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);

        x -= 1; // on avance d'une case en haut
        y += 1; // on avance d'une case à droite
    }

    // Tant qu'on rencontre un marqueur
    while (x >= 0 && y < COLONNES                 // Tant qu'on n'est pas sorti de la grille
           && estPossible(grille_jeu[x][y])       // que la case est jouable (pas d'anneau et jouable)
           && possedeMarqueur(grille_jeu[x][y])) // et que la case possède un marqueur
    {
        x -= 1; // on avance d'une case en haut
        y += 1; // on avance d'une case à droite
    }

    // Si on n'est pas sorti de la grille, qu'on est pas sur un anneau et que la case est jouable
    if (x >= 0 && y < COLONNES && estPossible(grille_jeu[x][y]))
    {
        /* On ajoute le coup à la liste des coups possibles */
        liste_coups_t *coup = creerCoup(x_init, y_init, x, y);
        ajouterCoup(coups_possibles, coup);
    }
}

/*********************************************************************/
/* genereCoupAnneau()                                                */
/*                                                                   */
/* Génère les coups possibles pour un anneau à une position donnée   */
/*                                                                   */
/* En entree:                                                        */
/*      case_t ** grille_jeu : grille de jeu                         */
/*      int x : ligne de l'anneau                                    */
/*      int y : colonne de l'anneau                                  */
/*      liste_coups_t ** coups_possibles : liste des coups possibles */
/*                                                                   */
/* En sortie:                                                        */
/*                                                                   */
/*********************************************************************/
void genereCoupAnneau(case_t ** grille_jeu, int x, int y, liste_coups_t *coups_possibles)
{
    /* si on transforme la grille en un tableau 2D, on ne peut ni aller en haut à droite, ni en bas à gauche */
    int direction; // 0 : haut, 1 : haut-droite, 2 : droite, 3 : bas, 4 : bas-gauche, 5 : gauche

    /* Pour chaque direction */
    for (direction = 0; direction < 6; direction++)
    {
        /* En fonction de la direction */
        switch (direction)
        {
        case 0: // en haut
            genereCoupAnneauHaut(grille_jeu, x - 1, y, coups_possibles);
            break;

        case 1: // en haut à droite
            genereCoupAnneauHautDroite(grille_jeu, x - 1, y + 1, coups_possibles);
            break;
            
        case 2: // à droite
            genereCoupAnneauDroite(grille_jeu, x, y + 1, coups_possibles);
            break;

        case 3: // en bas
            genereCoupAnneauBas(grille_jeu, x + 1, y, coups_possibles);
            break;

        case 4: // en bas à gauche
            genereCoupAnneauBasGauche(grille_jeu, x + 1, y - 1, coups_possibles);
            break;

        case 5: // à gauche
            genereCoupAnneauGauche(grille_jeu, x, y - 1, coups_possibles);
            break;

        default:
            break;
        }
    }
}

/*********************************************************************/
/* genereCoups()                                                     */
/*                                                                   */
/* Génère la liste des coups possibles pour un joueur à une position */
/* donnée                                                            */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : position de jeu à évaluer                    */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*      liste_coups_t * : liste des coups possibles                  */
/*                                                                   */
/*********************************************************************/
liste_coups_t *genereCoups(jeu * mon_jeu, int joueur)
{
    /* On crée la liste de coups possibles */
    liste_coups_t *coups_possibles = creerListeCoups();

    /* Si c'est à MAX de jouer */
    if(joueur == 0)
    {
        /* On parcourt le tableau des positions des anneaux de MAX */
        for (int i_anneau = 0; i_anneau < 5; i_anneau++)
        {
            if (mon_jeu->anneaux_noirs[i_anneau][0] != -1)
                genereCoupAnneau(mon_jeu->plateau, mon_jeu->anneaux_noirs[i_anneau][0], mon_jeu->anneaux_noirs[i_anneau][1], coups_possibles);    
        }
    }
    /* Sinon si c'est à MIN de jouer */
    else
    {
        /* On parcourt le tableau des positions des anneaux de MIN */
        for (int i_anneau = 0; i_anneau < 5; i_anneau++)
        {
            if (mon_jeu->anneaux_blancs[i_anneau][0] != -1)
                genereCoupAnneau(mon_jeu->plateau, mon_jeu->anneaux_blancs[i_anneau][0], mon_jeu->anneaux_blancs[i_anneau][1], coups_possibles);    
        }
    }

    return coups_possibles;
}



/*********************************************************************/
/*                                                                   */
/*                       evaluationPosition                          */
/*                                                                   */
/*********************************************************************/

/*********************************************************************/
/* estPositionTerminale()                                            */
/*                                                                   */
/* Vérifie si la position est terminale                              */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : jeu à évaluer                                */
/*                                                                   */
/* En sortie:                                                        */
/*      int : 1 si la position est terminale, 0 sinon                */
/*                                                                   */
/*********************************************************************/
int estPositionTerminale(jeu * mon_jeu)
{
    /* On compte le nombre d'anneaux encore présents dans anneaux_blancs et anneaux_noirs */
    int nb_anneaux_blancs = 0;
    int nb_anneaux_noirs = 0;
    for (int i = 0; i < 5; i++)
    {
        if (mon_jeu->anneaux_blancs[i][0] != -1)
        {
            nb_anneaux_blancs++;
        }
        if (mon_jeu->anneaux_noirs[i][0] != -1)
        {
            nb_anneaux_noirs++;
        }
    }
    /* Si il reste que deux anneaux, alors le joueur a gagné */
    if (nb_anneaux_blancs == 2 || nb_anneaux_noirs == 2)
    {
        return 1;
    }
    /* Sinon le joueur a perdu */
    else{
        return 0;
    }
}

/*********************************************************************/
/* calculScorePosition()                                             */
/*                                                                   */
/* Calcule le score d'une position de jeu                            */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : jeu à évaluer                                */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*      int : score de la position                                   */
/*                                                                   */
/*********************************************************************/
int calculScorePositionAvecAnneaux(jeu * mon_jeu, int joueur)
{   
    int score = 0;
    /* Si c'est à MAX de jouer */
    if(joueur == 0)
    {
        // printf("k");
        int nb_anneaux_noirs = nbAnneauxGagnes(mon_jeu, joueur);
        // printf("a\n");
        score += nb_anneaux_noirs * 100;
        score += (mon_jeu->nb_marqueurs_noirs - mon_jeu->nb_marqueurs_blancs) * 10;
    }
    /* Sinon si c'est à MIN de jouer */
    else
    {
        // printf("v");
        int nb_anneaux_blancs = nbAnneauxGagnes(mon_jeu, joueur);
        // printf("b\n");
        score += nb_anneaux_blancs * 100;
        score += (mon_jeu->nb_marqueurs_blancs - mon_jeu->nb_marqueurs_noirs) * 10;
    }

    return score;
}

int calculScorePositionSansAnneaux(jeu * mon_jeu, int joueur)
{   
    int score = 0;
    /* Si c'est à MAX de jouer */
    if(joueur == 0)
    {
        score += (mon_jeu->nb_marqueurs_noirs - mon_jeu->nb_marqueurs_blancs) * 10;
    }
    /* Sinon si c'est à MIN de jouer */
    else
    {
        score += (mon_jeu->nb_marqueurs_blancs - mon_jeu->nb_marqueurs_noirs) * 10;
    }

    return score;
}

/*********************************************************************/
/* evaluationMAX()                                                   */
/*                                                                   */
/* Evalue le score d'une position de jeu par rapport à MAX           */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : jeu à évaluer                                */
/*      int ite : profondeur actuelle                                */
/*      int joueur : joueur actuel                                   */
/*                                                                   */
/* En sortie:                                                        */
/*      int : score de la position par rapport à MAX                 */
/*                                                                   */
/*********************************************************************/
int evaluationMAX(jeu * mon_jeu, int ite, int joueur, int (*calculScorePosition)(jeu *, int))
{
    if (estPositionTerminale(mon_jeu)) // Si MAX a gagné
    {
        return 1000;
    }
    else if (ite >= PROF_MAX) // Si la profondeur maximale est atteinte
    {
        return calculScorePosition(mon_jeu, joueur);
    }
    else
    {
        /* Recuperation de la liste des coups possibles de MIN */
        liste_coups_t * coups_possibles = genereCoups(mon_jeu, !joueur);
        /* Creation des variables pour garder le coup maximal */
        int meilleur_score = -1; // On initialise le meilleur score à -1
        /* Parcours de la liste des coups possibles de MIN */

        liste_coups_t * coup = coups_possibles->suiv;
        if (coup != NULL)
        {
            /* On simule le coup */
            mon_jeu = deplaceAnneau(mon_jeu, !joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
            /* On garde le meilleur score */
            meilleur_score = evaluationMIN(mon_jeu, ite + 1, !joueur, (*calculScorePosition));
            /* On desapplique le coup */
            mon_jeu = desappliqueDeplaceAnneau(mon_jeu, !joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
            /* On passe au coup suivant */
            coup = coup->suiv;
        }

        while (coup != NULL)
        {
            /* On simule le coup de MIN */
            mon_jeu = deplaceAnneau(mon_jeu, !joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
            /* On évalue la position */
            int score = evaluationMIN(mon_jeu, ite + 1, !joueur, (*calculScorePosition));
            /* On garde le meilleur score */
            if (score > meilleur_score)
            {
                meilleur_score = score;
            }
            /* On désapplique le coup de MIN */
            mon_jeu = desappliqueDeplaceAnneau(mon_jeu, !joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
            /* On passe au coup suivant */
            coup = coup->suiv;
        }

        libererListeCoups(coups_possibles);

        return meilleur_score;
    }
}

/*********************************************************************/
/* evaluationMIN()                                                   */
/*                                                                   */
/* Evalue le score d'une position de jeu par rapport à MIN           */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : jeu à évaluer                                */
/*      int ite : profondeur actuelle                                */
/*                                                                   */
/* En sortie:                                                        */
/*      int : score de la position par rapport à MIN                 */
/*                                                                   */
/*********************************************************************/
int evaluationMIN(jeu * mon_jeu, int ite, int joueur, int (*calculScorePosition)(jeu *, int))
{
    return 0 - evaluationMAX(mon_jeu, ite, joueur, (*calculScorePosition));
}

/*********************************************************************/
/* evaluationPosition()                                              */
/*                                                                   */
/* Evalue le score d'une position de jeu par rapport à MAX           */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : jeu à évaluer                                */
/*                                                                   */
/* En sortie:                                                        */
/*      int : score de la position par rapport à MAX                 */
/*                                                                   */
/*********************************************************************/
int evaluationPosition(jeu * mon_jeu, int joueur, int (*calculScorePosition)(jeu *, int))
{
    return evaluationMAX(mon_jeu, 0, joueur, (*calculScorePosition));
}



/*********************************************************************/
/*                                                                   */
/*                            choisirCoup                            */
/*                                                                   */
/*********************************************************************/

/*********************************************************************/
/* choisirCoup()                                                     */
/*                                                                   */
/* Choisit le meilleur coup possible pour MAX                        */
/*                                                                   */
/* En entree:                                                        */
/*      jeu * mon_jeu : jeu à évaluer                                */
/*                                                                   */
/* En sortie:                                                        */
/*      FILE * : fichier contenant le meilleur coup possible         */
/*                                                                   */
/*********************************************************************/
void choisirCoup(jeu * mon_jeu, int joueur, int (*calculScorePosition)(jeu *, int))
{
    /* On récupère la liste des coups possibles depuis une position */
    liste_coups_t * coups_possibles = genereCoups(mon_jeu, joueur);
    /* On evalue chaque coup et on recupere celui qui a le meilleur score garantit */
    liste_coups_t * coup = coups_possibles->suiv;
    int meilleur_score;
    liste_coups_t * meilleur_coup = NULL;
    if (coup != NULL)
    {
        /* On simule le coup */
        mon_jeu = deplaceAnneau(mon_jeu, joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
        /* On garde le meilleur score */
        meilleur_coup = coup;
        meilleur_score = evaluationPosition(mon_jeu, joueur, calculScorePosition);
        /* On desapplique le coup */
        mon_jeu = desappliqueDeplaceAnneau(mon_jeu, joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
        coup = coup->suiv;
    }
    while (coup != NULL)
    {
        /* On simule le coup */
        mon_jeu = deplaceAnneau(mon_jeu, joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
        /* On evalue la position */
        int score = evaluationPosition(mon_jeu, joueur, calculScorePosition);
        /* On garde le meilleur score */
        if (score > meilleur_score)
        {
            meilleur_coup = coup;
            meilleur_score = score;
        }
        // printf("Score garantit : %d\n", score);
        /* On desapplique le coup */
        mon_jeu = desappliqueDeplaceAnneau(mon_jeu, joueur, coup->x_origine, coup->y_origine, coup->x_fin, coup->y_fin);
        // afficheCoup(coup);
        /* On passe au coup suivant */
        coup = coup->suiv;
    }

    /* On renvoie le meilleur coup sous forme de fichier */
    FILE * fichier = fopen("coup.txt", "w");
    fprintf(fichier, "%d,%d;%d,%d", meilleur_coup->x_origine, meilleur_coup->y_origine, meilleur_coup->x_fin, meilleur_coup->y_fin);
    fclose(fichier);

    libererListeCoups(coups_possibles);
}