#include <stdio.h>
#include <stdlib.h>

#include "fenetre.h"

#include <SDL2/SDL.h>

/*********************************************************************************/
/* Fonction changePosition                                                       */
/*                                                                               */
/* Fonction permettant de changer la position de la fenêtre                      */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - fenetre : pointeur sur la structure donnees_f_t                        */
/*      - cas_position : entier indiquant la position de la fenêtre              */
/*      - w_ecran : largeur de l'écran                                           */
/*      - h_ecran : hauteur de l'écran                                           */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void changePosition(donnees_f_t *fenetre, int cas_position, int w_ecran, int h_ecran)
{
    switch (cas_position)
    {
    case 0: // en haut à gauche
        SDL_SetWindowPosition(fenetre->window, 0, 0);
        break;

    case 1: // en haut à droite
        SDL_SetWindowPosition(fenetre->window, w_ecran / 2, 0);
        break;

    case 2: // en bas à gauche
        SDL_SetWindowPosition(fenetre->window, 0, h_ecran / 2);
        break;

    case 3: // en bas à droite
        SDL_SetWindowPosition(fenetre->window, w_ecran / 2, h_ecran / 2);
        break;

    default:
        break;
    }
}

/*********************************************************************************/
/* Fonction changeTaille                                                         */
/*                                                                               */
/* Fonction permettant de changer la taille de la fenêtre                        */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - fenetre : pointeur sur la structure donnees_f_t                        */
/*      - cas_taille : entier indiquant le changement de taille                   */
/*      - w_ecran : largeur de l'écran                                           */
/*      - h_ecran : hauteur de l'écran                                           */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void changeTaille(donnees_f_t *fenetre, int cas_taille, int w_ecran, int h_ecran)
{
    if (cas_taille == 0)
    { // cas augmentation largeur
        if (fenetre->width < w_ecran / 2)
        { // borne/limite d'augmentation
            fenetre->width += 5;
        }
    }

    else if (cas_taille == 1)
    { // cas diminution largeur
        if (fenetre->width >= 200)
        { // borne/limite de diminution
            fenetre->width -= 5;
        }
    }
    else if (cas_taille == 2)
    { // cas augmentation hauteur
        if (fenetre->height < h_ecran / 2)
        { // borne/limite d'augmentation'
            fenetre->height += 5;
        }
    }
    else
    { // cas diminution hauteur
        if (fenetre->height >= 50)
        { // borne/limite de diminution
            fenetre->height -= 5;
        }
    }

    SDL_SetWindowSize(fenetre->window, fenetre->width, fenetre->height);
}

/*********************************************************************************/
/* Fonction afficheFenetre                                                       */
/*                                                                               */
/* Fonction permettant d'afficher une fenêtre                                    */
/*                                                                               */
/* Paramètres :                                                                  */
/*      - void                                                                   */
/*                                                                               */
/* Retour : void                                                                 */
/*                                                                               */
/*********************************************************************************/
void afficheFenetre()
{
    donnees_f_t ma_fenetre;
    donnees_f_t ma_nouv_fenetre;
    int graphics = 1;

    SDL_DisplayMode mode;

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
    { // initialisation SDL
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        graphics = 0;
    }
    else
    {
        SDL_GetCurrentDisplayMode(0, &mode); // données de l'ecran

        ma_fenetre.width = mode.w / 2;  // moitie de l'ecran en largeur
        ma_fenetre.height = mode.h / 2; // moitie de l'ecran en hauteur

        ma_fenetre.window = SDL_CreateWindow("X Fenetre", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                             ma_fenetre.width, ma_fenetre.height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

        if (ma_fenetre.window == 0)
        {
            fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
            graphics = 0;
        }
    }

    if (graphics == 0)
    {
        if (ma_fenetre.window != 0)
            SDL_DestroyWindow(ma_fenetre.window);

        SDL_Quit();
        return;
    }

    SDL_bool
        program_on = SDL_TRUE,
        change_pos = SDL_FALSE,
        augmente_largeur = SDL_FALSE,
        diminue_largeur = SDL_FALSE,
        augmente_hauteur = SDL_FALSE,
        diminue_hauteur = SDL_FALSE,
        creer_nouv_fenetre = SDL_FALSE,
        suppr_nouv_fenetre = SDL_FALSE,
        event_utile = SDL_FALSE;
    SDL_Event event;

    int est_ouverte_nouv = 0; // booleen pour eviter d'ouvrir la fenetre si elle est deja ouverte

    int cas_position = 0; // de 0 à 3

    while (program_on)
    { // La boucle des évènements
        event_utile = SDL_FALSE;
        while (!event_utile && SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT: // on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE;
                event_utile = SDL_TRUE;
                break;

            case SDL_KEYDOWN: // Le type de event est : une touche appuyée

                switch (event.key.keysym.sym)
                {
                case SDLK_SPACE:              // 'SPACE'
                    change_pos = !change_pos; // basculement change_pos/unchange_pos
                    event_utile = SDL_TRUE;
                    break;

                case SDLK_RIGHT:                          // 'RIGHT'
                    augmente_largeur = !augmente_largeur; // on augmente la largeur de la fenetre
                    event_utile = SDL_TRUE;
                    break;

                case SDLK_LEFT:                         // 'LEFT'
                    diminue_largeur = !diminue_largeur; // on diminue la largeur de la fenetre
                    event_utile = SDL_TRUE;
                    break;

                case SDLK_UP:                             // 'UP'
                    augmente_hauteur = !augmente_hauteur; // on augmente la largeur de la fenetre
                    event_utile = SDL_TRUE;
                    break;

                case SDLK_DOWN:                         // 'DOWN'
                    diminue_hauteur = !diminue_hauteur; // on diminue la largeur de la fenetre
                    event_utile = SDL_TRUE;
                    break;

                case SDLK_a: // 'a'
                    creer_nouv_fenetre = !creer_nouv_fenetre;
                    event_utile = SDL_TRUE;
                    break;

                case SDLK_z: // 'z'
                    suppr_nouv_fenetre = !suppr_nouv_fenetre;
                    event_utile = SDL_TRUE;
                    break;

                default: // Une touche appuyée qu'on ne traite pas
                    break;
                }
                break;

            case SDL_MOUSEBUTTONDOWN: // Click souris
                break;

            default: // Les évènements qu'on n'a pas envisagé
                break;
            }
        }

        if (!change_pos)
        { // changement de position, on passe à la suivante
            changePosition(&ma_fenetre, cas_position, mode.w, mode.h);
            cas_position = (cas_position + 1) % 4; // incrementaion de cas_position
            change_pos = !change_pos;              // basculement change_pos/unchange_pos
        }

        if (!augmente_largeur)
        { // augmentation de la hauteur
            changeTaille(&ma_fenetre, 0, mode.w, mode.h);
            augmente_largeur = !augmente_largeur;
        }

        if (!diminue_largeur)
        { // diminution de la largeur
            changeTaille(&ma_fenetre, 1, mode.w, mode.h);
            diminue_largeur = !diminue_largeur;
        }

        if (!augmente_hauteur)
        { // augmentation de la hauteur
            changeTaille(&ma_fenetre, 2, mode.w, mode.h);
            augmente_hauteur = !augmente_hauteur;
        }

        if (!diminue_hauteur)
        { // diminution de la hauteur
            changeTaille(&ma_fenetre, 3, mode.w, mode.h);
            diminue_hauteur = !diminue_hauteur;
        }

        if (!creer_nouv_fenetre)
        {
            if (!est_ouverte_nouv)
            {
                // creation d'une nouvelle fenetre à l'opposé de la premiere
                // copie des dimensions de la premiere fenetre
                ma_nouv_fenetre.width = ma_fenetre.width;
                ma_nouv_fenetre.height = ma_fenetre.height;
                // creation de la fenetre
                ma_nouv_fenetre.window = SDL_CreateWindow("X Fenetre", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                                          ma_nouv_fenetre.width, ma_nouv_fenetre.height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

                if (ma_nouv_fenetre.window == 0)
                {
                    fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
                    graphics = 0;
                }
                else
                { // on la place à l'opposé de la premiere
                    // on recupere la positon de l'autre fenetre
                    int x_fenetre, y_fenetre;
                    SDL_GetWindowPosition(ma_fenetre.window, &x_fenetre, &y_fenetre);

                    // on entre la position de la nouvelle fenetre
                    int x_nouv_fenetre, y_nouv_fenetre;
                    if (x_fenetre <= 100)
                    {                                // si la fenetre originale est à gauche
                        x_nouv_fenetre = mode.w / 2; // la nouv est a droite
                    }
                    else
                    {                       // si la fenetre originale est à droite
                        x_nouv_fenetre = 0; // la nouv est a gauche
                    }

                    if (y_fenetre <= 100)
                    {                                // si la fenetre originale est en haut
                        y_nouv_fenetre = mode.h / 2; // la nouv est en bas
                    }
                    else
                    {                       // si la fenetre originale est en bas
                        y_nouv_fenetre = 0; // la nouv est en haut
                    }

                    SDL_SetWindowPosition(ma_nouv_fenetre.window, x_nouv_fenetre, y_nouv_fenetre);
                    est_ouverte_nouv = 1;
                }
            }
            creer_nouv_fenetre = !creer_nouv_fenetre;
        }

        if (!suppr_nouv_fenetre)
        {
            if (est_ouverte_nouv)
            {
                // suppression de la nouvelle fenetre
                SDL_DestroyWindow(ma_nouv_fenetre.window);
                est_ouverte_nouv = 0;
            }
            suppr_nouv_fenetre = !suppr_nouv_fenetre;
        }

        SDL_Delay(50); // Petite pause
    }

    SDL_DestroyWindow(ma_fenetre.window);
    SDL_Quit();
}