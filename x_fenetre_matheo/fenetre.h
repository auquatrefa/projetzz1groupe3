#ifndef fenetre_h
#define fenetre_h

#include <SDL2/SDL.h>

typedef struct donnees_fenetre_s
{
    SDL_Window *window;
    int width;
    int height;
} donnees_f_t;


void afficheFenetre();

#endif